import os
import zlib
import logging
import prettytable as pt

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")

STOCK_LOG_PATH_STR = 'STOCK_LOG_PATH'

DEFAULT_FEATURE_MAT_DIR_STR = 'FEATURE_MAT_DIRS'
DEFAULT_FEATURE_MAT_DIR = os.path.join(STOCK_LIB_PATH, 'data', 'mat')


def get_feature_mat_dirs():
    return os.getenv(DEFAULT_FEATURE_MAT_DIR_STR, DEFAULT_FEATURE_MAT_DIR).split(";")


DEFAULT_DATA_MANAGER_DIR_STR = 'DATA_MANAGER_DIRS'
DEFAULT_DATA_MANAGER_DIR = os.path.join(STOCK_LIB_PATH, 'data', 'data_manager')


def get_data_manager_dir():
    return os.getenv(DEFAULT_DATA_MANAGER_DIR_STR, DEFAULT_DATA_MANAGER_DIR)


def encode_str(str_in):
    return str(zlib.adler32(str_in.encode('utf-8')))


def create_logger(fpath):
    logger_fmt = logging.Formatter(
        '[%(asctime)s] %(filename)s:%(lineno)s - %(funcName)s() %(levelname)s : %(message)s')
    logger = logging.getLogger()
    stream_logger = logging.StreamHandler()
    stream_logger.setFormatter(logger_fmt)
    logger.addHandler(stream_logger)
    if len(fpath) > 0:
        file_logger = logging.FileHandler(
            fpath, mode='a')
        file_logger.setFormatter(logger_fmt)
        logger.addHandler(file_logger)
    return logger


def print_metrics(test_start_day, metrics, other_data={}):
    tb = pt.PrettyTable()
    field_names = ["test_start_day",
                   "N",
                   "avg_nnz",
                   "rho_u",
                   "sharpe_u",
                   "rho_in_u",
                   "nw_beta_u",
                   "nw_tstat_u"]
    field_names = field_names + list(other_data.keys())

    tb.field_names = field_names

    row = [test_start_day,
           metrics['nstocks'],
           metrics['avg_nnz'],
           round(metrics['rho_u'], 4),
           round(metrics['sharpe_u'], 4),
           round(metrics['rho_iu'], 4),
           round(metrics['nwbeta_u'], 4),
           round(metrics['nwts_u'], 4)
           ]

    for key in list(other_data.keys()):
        row.append(other_data[key])

    tb.add_row(row)

    print(tb)


def print_metrics_list(test_start_days, metrics_list, other_data=[]):
    assert(len(test_start_days) == len(metrics_list))
    tb = pt.PrettyTable()
    field_names = ["test_start_day",
                   "N",
                   "avg_nnz",
                   "rho_u",
                   "sharpe_u",
                   "rho_in_u",
                   "nw_beta_u",
                   "nw_tstat_u"]

    if len(other_data) == 0:
        other_data_key = []
    else:
        other_data_key = list(other_data[0].keys())

    field_names = field_names + other_data_key
    tb.field_names = field_names

    for idx in range(0, len(test_start_days)):
        test_start_day = test_start_days[idx]
        metrics = metrics_list[idx]
        row = [test_start_day,
               metrics['nstocks'],
               metrics['avg_nnz'],
               round(metrics['rho_u'], 4),
               round(metrics['sharpe_u'], 4),
               round(metrics['rho_iu'], 4),
               round(metrics['nwbeta_u'], 4),
               round(metrics['nwts_u'], 4)
               ]
        for key in other_data_key:
            row.append(other_data[idx][key])
        tb.add_row(row)

    print(tb)


LOGGER = create_logger(os.getenv(STOCK_LOG_PATH_STR, ""))

BLACK_STOCKS_HEAD = ('ATEST', 'NTEST', 'PTEST', 'SPY', 'ZXZZT')

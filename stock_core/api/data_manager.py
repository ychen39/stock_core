import os
import json
import copy
from functools import reduce
import pandas as pd
import numpy as np
from stock_core.features import FeatureCenter
from stock_core.filters import StockFilterWrapper
from stock_core.sys_utils import get_data_manager_dir, encode_str, BLACK_STOCKS_HEAD


def get_topk_liquid_data(start_date, end_date, sadv_date, universe_size, names):
    f = FeatureCenter()
    dfs = []

    if isinstance(names, list) == False:
        names = [names]

    sadv = f.get_expr_feature('sadv_past_1y')
    sfw = StockFilterWrapper(sadv.get_stocks())
    sadv = sadv.get_feature_data_lastday(sadv_date)
    sfw.update_mask('rank', (sadv, 'max', universe_size))

    for name in names:
        name = name.replace(" ", "")
        df = f.get_expr_feature(name).get_feature_data(start_date, end_date)
        df = sfw.filter(df)
        df = pd.melt(df.reset_index(), id_vars='index')
        df = df[['variable', 'index', 'value']].rename(
            columns={'variable': 'date', 'index': 'stock', 'value': name})
        dfs.append(df)

    return reduce(lambda df_left, df_right:
                  df_left.merge(df_right, on=['date', 'stock'], validate="one_to_one"), dfs)


def get_all_nan_stocks(df, data_names):
    groupby_count = df.groupby('stock').agg('count')
    mask = None
    for name in data_names:
        name = name.replace(" ", "")
        if mask is None:
            mask = groupby_count[name] == 0
        else:
            mask = mask | (groupby_count[name] == 0)
    return groupby_count[mask].index.values


def remove_junky_stocks(train_df, test_df, data_names):
    stock_to_remove = tuple(np.union1d(get_all_nan_stocks(
        train_df, data_names), get_all_nan_stocks(test_df, data_names)))
    train_df = train_df[~train_df['stock'].str.startswith(stock_to_remove)]
    test_df = test_df[~test_df['stock'].str.startswith(stock_to_remove)]

    train_df = train_df[~train_df['stock'].str.startswith(BLACK_STOCKS_HEAD)]
    test_df = test_df[~test_df['stock'].str.startswith(BLACK_STOCKS_HEAD)]
    return train_df, test_df


def pack_xy(df, y_name, x_names):
    y_name = y_name.replace(" ", "")
    x_names = [x_name.replace(" ", "") for x_name in x_names]
    y = df.pivot('stock', 'date', y_name)
    x_pivot = df.pivot('stock', 'date', x_names)
    if isinstance(x_names, list):
        x_list = [x_pivot[x_name].to_numpy() for x_name in x_names]
        x = np.concatenate(x_list, axis=0)
        return y.to_numpy(), x
    else:
        return y.to_numpy(), x_pivot.to_numpy()


def get_meta_data():
    meta_file = os.path.join(get_data_manager_dir(), 'meta.json')
    if os.path.exists(meta_file) == False:
        return {}
    with open(meta_file) as f_in:
        return json.load(f_in)


def sync_meta_data(data):
    os.makedirs(get_data_manager_dir(), exist_ok=True)
    meta_file = os.path.join(get_data_manager_dir(), 'meta.json')
    with open(meta_file, 'w') as f:
        json.dump(data, f)


def build_query_data(columns):
    columns = copy.deepcopy(columns)
    columns.sort()
    columns_str = ';'.join(columns).replace(" ", "")
    query_key = encode_str(columns_str)

    data = get_meta_data()
    if query_key in data:
        return query_key
    else:
        data[query_key] = columns_str.split(';')
        sync_meta_data(data)

    f = FeatureCenter()

    for column in columns_str.split(';'):
        coo_feat_path = os.path.join(
            get_data_manager_dir(), encode_str(column) + '_coo.pqt')
        if os.path.exists(coo_feat_path):
            continue
        df = f.get_expr_feature(column).get_raw_data()
        df = pd.melt(df.reset_index(), id_vars='index')
        df = df[['variable', 'index', 'value']].rename(
            columns={'variable': 'date', 'index': 'stock', 'value': column})
        df.to_parquet(coo_feat_path)

    return query_key


def get_data(start_date, end_date, query_key, sadv_date, universe_size, cache=True):
    param_str = ';'.join([str(start_date), str(end_date), str(
        query_key), str(sadv_date), str(universe_size)])
    param_encode_str = encode_str(param_str)
    cache_file = os.path.join(get_data_manager_dir(),
                              param_encode_str + "_cache.pqt")
    if os.path.exists(cache_file):
        df = pd.read_parquet(cache_file)
        df['date'] = df['date'].astype('object')
        return df

    f = FeatureCenter()
    sadv = f.get_expr_feature('sadv_past_1y')
    sfw = StockFilterWrapper(sadv.get_stocks())
    sadv = sadv.get_feature_data_lastday(sadv_date)
    sfw.update_mask('rank', (sadv, 'max', universe_size))
    universe_stocks = sfw.get_stocks()

    columns = get_meta_data()[str(query_key)]
    dfs = []
    for column in columns:
        coo_feat_path = os.path.join(
            get_data_manager_dir(), encode_str(column) + '_coo.pqt')
        df = pd.read_parquet(coo_feat_path)
        df = df[(df['date'] >= start_date) & (df['date'] < end_date)]
        df = df[df['stock'].isin(universe_stocks)]
        dfs.append(df)
    res = reduce(lambda df_left, df_right: df_left.merge(
        df_right, on=['date', 'stock']), dfs)

    if cache:
        res.to_parquet(cache_file)
    return res

def get_data_list():
    f = FeatureCenter()
    return list(f.get_feature_list())

from math import sqrt
import numpy as np
from stock_core import features, filters, model_runner, evaluator, sys_utils, utils


class Engine(object):

    def __init__(self):
        self.f = features.FeatureCenter()
        self.feat_objs = None
        self.train_response = None
        self.test_response = None
        self.universe_config = None
        self.model_list = None

        self.decorr_config = None
        self.post_process_type = None

        # tmp data
        self.sfw = None

        self.train_start_date = None
        self.train_end_date = None
        self.test_start_date = None
        self.test_end_date = None

        self.train_feat_dfs = None
        self.test_feat_dfs = None
        self.train_resp_df = None
        self.test_resp_df = None

    def features(self, features_names):
        self.feat_objs = {feature_name: self.f.get_expr_feature(
            feature_name) for feature_name in features_names}
        return self

    def responses(self, train_rsp_name, test_rsp_name):
        self.train_response = self.f.get_expr_feature(train_rsp_name)
        self.test_response = self.f.get_expr_feature(test_rsp_name)
        return self

    def universe(self, field, op='max', threadshold=3000):
        self.universe_config = (
            self.f.get_expr_feature(field), op, threadshold)
        return self

    def models(self, model_list):
        self.model_list = model_list
        return self

    def decorr_y(self, field, step=63):
        self.decorr_config = (self.f.get_expr_feature(field), step)
        return self

    def postprocs(self, postprocs_type):
        self.post_process_type = postprocs_type
        return self

    def filter_hook(self, filter_wrapper):
        pass

    def preprocs_hook(self, model_runner):
        pass

    def postprocs_hook(self, evaluator):
        pass

    def prepare_data(self, train_start_date, train_end_date, test_start_date, test_end_date):
        if None in [self.feat_objs,
                    self.train_response,
                    self.test_response,
                    self.universe_config]:
            raise ValueError("Required parameters not initialized")

        self.train_start_date = train_start_date
        self.train_end_date = train_end_date
        self.test_start_date = test_start_date
        self.test_end_date = test_end_date

        # Get train and test data based on date
        train_feat_dfs = {k: v.get_feature_data(
            train_start_date, train_end_date) for k, v in self.feat_objs.items()}
        test_feat_dfs = {k: v.get_feature_data(
            test_start_date, test_end_date) for k, v in self.feat_objs.items()}
        train_resp_df = self.train_response.get_feature_data(
            train_start_date, train_end_date)
        test_resp_df = self.test_response.get_feature_data(
            test_start_date, test_end_date)

        # Init filter wrapper with stocks
        stocks = self.train_response.get_stocks()
        self.sfw = filters.StockFilterWrapper(stocks)

        # Mark all nan or blacklist stocks
        self.sfw.update_mask('dropnan', list(train_feat_dfs.values()) +
                             list(test_feat_dfs.values()) + [train_resp_df, test_resp_df])
        self.sfw.update_mask('BL', (stocks, ))

        # Mark stocks based on universe
        universe_field = self.universe_config[0].get_feature_data_lastday(
            train_end_date)
        self.sfw.update_mask(
            'rank', (universe_field, self.universe_config[1], self.universe_config[2]))

        # User define hook for filter
        self.filter_hook(self.sfw)

        # Use filter to filter all data
        self.train_feat_dfs = {k: self.sfw.filter(v)
                               for k, v in train_feat_dfs.items()}
        self.test_feat_dfs = {k: self.sfw.filter(v)
                              for k, v in test_feat_dfs.items()}
        self.train_resp_df = self.sfw.filter(train_resp_df)
        self.test_resp_df = self.sfw.filter(test_resp_df)

    def clean_data(self):
        self.sfw = None

        self.train_start_date = None
        self.train_end_date = None
        self.test_start_date = None
        self.test_end_date = None

        self.train_feat_dfs = None
        self.test_feat_dfs = None
        self.train_resp_df = None
        self.test_resp_df = None

    def run_models(self, model_configs=None):
        if self.model_list is None:
            raise ValueError("Required parameters not initialized")

        if model_configs is None:
            model_configs = [None] * len(self.model_list)

        # Init model runner
        p = model_runner.ModelRunner(self.train_resp_df,
                                     self.train_feat_dfs.values(),
                                     self.test_resp_df,
                                     self.test_feat_dfs.values(),
                                     self.model_list,
                                     model_configs)

        # Decorr Y if it's set
        if self.decorr_config is not None:
            decorr_train_field = self.decorr_config[0].get_feature_data(
                self.train_start_date, self.train_end_date)
            decorr_test_field = self.decorr_config[0].get_feature_data(
                self.test_start_date, self.test_end_date)
            decorr_train_field, decorr_test_field = self.sfw.filter(
                decorr_train_field, decorr_test_field)
            p.execute_preprocs('decorr', (decorr_train_field,
                                          decorr_test_field, self.decorr_config[1]))

        # Execute user-defined preprocess
        self.preprocs_hook(p)

        # Run model
        p.run()
        model_output = p.get_combined_result()

        # Init evaluator
        t = evaluator.Evaluator(model_output)
        if self.post_process_type != None:
            t.execute_postprocs(self.post_process_type)

        # Execute user-defined post process
        self.postprocs_hook(t)

        t.evaluate()
        metrics = t.get_metrics()
        metrics['stock_mask'] = self.sfw.get_mask()
        return model_output, metrics

    def single_run(self,
                   train_start_date,
                   train_end_date,
                   test_start_date,
                   test_end_date,
                   model_configs=None):
        if None in [self.feat_objs,
                    self.train_response,
                    self.test_response,
                    self.universe_config,
                    self.model_list]:
            raise ValueError("Required parameters not initialized")

        self.prepare_data(train_start_date, train_end_date,
                          test_start_date, test_end_date)

        model_output, metrics = self.run_models(model_configs)

        self.clean_data()

        return model_output, metrics

    def run(self, date_config, model_configs=None):
        raw_metrics = []
        raw_outputs = []

        if model_configs is None:
            model_configs = [None] * len(date_config)
        idx = 0

        for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in date_config:
            sys_utils.LOGGER.info('Running: train->(%d, %d) test->(%d, %d)' %
                                  (tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt))
            # print('Running: train->(%d, %d) test->(%d, %d)' %
            #   (tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt))
            model_output, metrics = self.single_run(
                tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt, model_configs[idx])
            raw_metrics.append(metrics)
            raw_outputs.append(model_output)

            idx += 1

        combined_metrics = {}
        for i in range(0, len(date_config)):
            for k, v in raw_metrics[i].items():
                if k not in combined_metrics:
                    combined_metrics[k] = []
                combined_metrics[k].append(v)

        rg_rho_u = np.asarray(combined_metrics['rho_u'])
        rg_rho_iu = np.asarray(combined_metrics['rho_iu'])
        r_u_glued = np.concatenate(combined_metrics['u_glued'])
        nw_rgbeta_u_g = np.concatenate(combined_metrics['nw_rgbeta_u_g'])

        rho_u_glued = np.mean(rg_rho_u)
        rho_iu_glued = np.mean(rg_rho_iu)
        sharpe_u_glued = np.mean(r_u_glued) / np.std(r_u_glued) * sqrt(252)
        nwbeta_u_glued, nwse_u_glued, nwts_u_glued = utils.neweywest(
            nw_rgbeta_u_g)

        metrics_glude = {}
        metrics_glude['rho_u'] = rho_u_glued
        metrics_glude['rho_iu'] = rho_iu_glued
        metrics_glude['sharpe_u'] = sharpe_u_glued
        metrics_glude['nwbeta_u'] = nwbeta_u_glued
        metrics_glude['nwse_u'] = nwse_u_glued
        metrics_glude['nwts_u'] = nwts_u_glued

        avg_nnz_glude = np.asarray(combined_metrics['avg_nnz'])
        metrics_glude['avg_nnz'] = np.mean(avg_nnz_glude)
        stock_mask = np.zeros_like(combined_metrics['stock_mask'][0])
        for mask in combined_metrics['stock_mask']:
            stock_mask = stock_mask | mask
        metrics_glude['nstocks'] = np.sum(stock_mask[stock_mask > 0])

        raw_metrics.append(metrics_glude)
        return raw_outputs, raw_metrics

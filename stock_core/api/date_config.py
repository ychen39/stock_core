from datetime import datetime
from dateutil.relativedelta import relativedelta


class DateConfig(object):

    def __init__(self,
                 first_day,
                 last_day,
                 train_years=0,
                 train_months=0,
                 gray_years=0,
                 gray_months=0,
                 test_years=0,
                 test_months=0):
        self.train_start_dates = []
        self.train_end_dates = []
        self.test_start_dates = []
        self.test_end_dates = []
        self.iter_idx = 0

        first_day_str = str(first_day)
        last_day_str = str(last_day)
        first_day_obj = datetime(year=int(first_day_str[0:4]), month=int(
            first_day_str[4:6]), day=int(first_day_str[6:8]))
        last_day_obj = datetime(year=int(last_day_str[0:4]), month=int(
            last_day_str[4:6]), day=int(last_day_str[6:8]))

        train_window = relativedelta(years=train_years, months=train_months)
        gray_window = relativedelta(years=gray_years, months=gray_months)
        test_window = relativedelta(years=test_years, months=test_months)

        train_start_date = first_day_obj
        test_start_date = train_start_date + train_window + gray_window
        test_end_date = test_start_date + test_window

        while test_end_date <= last_day_obj:
            self.test_start_dates.append(
                int(test_start_date.strftime("%Y%m%d")))
            self.test_end_dates.append(int(test_end_date.strftime("%Y%m%d")))

            self.train_start_dates.append(
                int((test_start_date - gray_window - train_window).strftime("%Y%m%d")))
            self.train_end_dates.append(
                int((test_start_date - gray_window).strftime("%Y%m%d")))

            test_start_date += test_window
            test_end_date += test_window

    def get_train_start_dates(self):
        return self.train_start_dates

    def get_train_end_dates(self):
        return self.train_end_dates

    def get_test_start_dates(self):
        return self.test_start_dates

    def get_test_end_dates(self):
        return self.test_end_dates

    def __iter__(self):
        return self

    def __next__(self):
        if self.iter_idx == len(self.train_start_dates):
            self.iter_idx = 0
            raise StopIteration
        self.iter_idx += 1
        return (self.train_start_dates[self.iter_idx-1],
                self.train_end_dates[self.iter_idx-1],
                self.test_start_dates[self.iter_idx-1],
                self.test_end_dates[self.iter_idx-1])

    def __len__(self):
        return len(self.train_start_dates)


# def main():
#     a = DateConfig(20071201, 20190101, test_months=3,
#                    gray_months=1, train_years=3)
#     for a, b, c, d in a:
#         print(a, b, c, d)


# main()

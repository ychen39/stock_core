from math import sqrt
import typing
from dataclasses import dataclass
from scipy.stats import pearsonr, kendalltau
import numpy as np


@dataclass
class ModelInput:
    train_y: np.ndarray
    train_x: np.ndarray
    test_y: np.ndarray
    test_x: np.ndarray


@dataclass
class ModelOutput:
    train_y: np.ndarray
    train_pred_y: np.ndarray
    test_y: np.ndarray
    test_pred_y: np.ndarray
    user_data: typing.Any = None


def decorr(y, x, step):
    z = np.empty(y.shape)
    z.fill(np.nan)
    start_idx = 0
    limit = y.shape[1]
    while start_idx < limit:
        end_idx = start_idx + step
        if end_idx >= limit:
            end_idx = limit
        x_tmp = x[:, start_idx:end_idx]
        y_tmp = y[:, start_idx:end_idx]
        count = x_tmp.shape[0] * x_tmp.shape[1]
        beta = np.linalg.lstsq(x_tmp.reshape(count, 1), y_tmp.reshape(
            count, 1), rcond=None)[0].reshape(1)
        z[:, start_idx:end_idx] = y_tmp - beta * x_tmp
        start_idx = end_idx
    return z


def pack_yx(df_y, *df_xs):
    y = df_y.to_numpy()
    x_list = [df_x.to_numpy() for df_x in df_xs]
    x = np.concatenate(x_list, axis=0)
    assert(len(x) % len(y) == 0)
    return y, x


def invest_simulate(signal, weight, ret):
    pos = signal * weight
    pos = pos / np.sum(np.absolute(pos), axis=0)
    r = pos * ret
    return np.sum(r, axis=0)


def corr_per_day(A, B, algo='Pearson'):
    days = A.shape[1]
    corrs = np.zeros(days)
    if algo == 'Kendall':
        corr = kendalltau
    else:
        corr = pearsonr
    for i in range(0, days):
        a_i = A[:, i]
        b_i = B[:, i]
        mask = ~(np.isnan(a_i) | np.isnan(b_i))
        corrs[i] = corr(a_i[mask], b_i[mask])[0]
    return corrs


def predy_beta(y, pred_y):
    days = y.shape[1]
    stocks = y.shape[0]
    rg_beta = np.zeros(days)
    for d in range(0, days):
        rg_beta[d] = np.linalg.lstsq(pred_y[:, d].reshape(
            stocks, 1), y[:, d].reshape(stocks, 1), rcond=None)[0].reshape(1)
    return rg_beta


def neweywest(ser):
    ser = ser[np.isfinite(ser)]
    T = ser.shape[0]
    L = np.floor(4.0 * (T / 100.0) ** (2.0 / 9))
    nwser = np.mean(ser)
    err = ser - np.ones(T) * nwser
    Q = 0
    for l in range(int(L) + 1):
        w_l = 1 - 1.0 * l / (L + 1)
        if l == 0:
            Q += np.sum(err ** 2)
        else:
            Q += 2 * w_l * np.dot(err[:-l], err[l:])
    nwse = np.sqrt(Q / T / (T - 1))
    ser_t = nwser / nwse
    return nwser, nwse, ser_t


def general_evaluate(test_pred_y, test_y):
    
    daily_r_u = invest_simulate(test_pred_y, np.ones_like(test_y), test_y)

    rho_u = np.mean(corr_per_day(test_pred_y, test_y, 'Pearson'))

    sharpe_u = np.mean(daily_r_u) / np.std(daily_r_u) * sqrt(252)

    rg_beta_u = predy_beta(test_y, test_pred_y)

    nwbeta_u, nwse_u, nwts_u = neweywest(rg_beta_u)

    metrics = {}
    metrics['rho_daily'] = corr_per_day(test_pred_y, test_y, 'Pearson')
    metrics['tau_daily'] = corr_per_day(test_pred_y, test_y, 'Kendall')
    metrics['u_glued'] = daily_r_u
    metrics['nw_rgbeta_u_g'] = rg_beta_u
    metrics['rho_u'] = rho_u
    metrics['sharpe_u'] = sharpe_u
    metrics['nwbeta_u'] = nwbeta_u
    metrics['nwse_u'] = nwse_u
    metrics['nwts_u'] = nwts_u

    metrics['avg_nnz'] = int(np.mean(np.count_nonzero(test_pred_y, axis=0)))

    return metrics

# def main():
#     from scipy import io as sio
#     mat = sio.loadmat("/stock_core/stock_core/matlab.mat")
#     x = mat['X']
#     y = mat['Y']
#     z = decorr(y, x, 63)
#     z_gt = mat['Z']
#     print(np.allclose(z, z_gt, equal_nan=True))

# def main():
#     import features
#     f = features.FeatureCenter()

#     y = f.get_expr_feature("M_fret1d - hdg_fret1d")
#     x1 = f.get_feature("M_ret2to4pm")
#     x2 = f.get_feature("M_sadv63d")
#     y_df = y.get_feature_data(20180202, 20190204)
#     x1_df = x1.get_feature_data(20180202, 20190204)
#     x2_df = x2.get_feature_data(20180202, 20190204)
#     y, x = pack_yx(y_df, x1_df, x2_df)
#     print(y.shape, x.shape)

# def main():
#     from scipy import io as sio
#     mat = sio.loadmat("/stock_core/test/data/invest_simullate1")
#     signal = mat['signal']
#     weight = mat['weight']
#     Mret = mat['Mret']
#     r = invest_simulate(signal, weight, Mret)
#     r_gt = mat['daily_r']
#     print(np.allclose(r, r_gt, equal_nan=True))


# def main():
#     from scipy import io as sio
#     mat = sio.loadmat(
#         "/stock_core/test/data/corr_per_day_k1.mat")
#     A = mat['A']
#     B = mat['B']
#     gt_C = mat['C']
#     C = corr_per_day(A, B, 'Kendall')
#     print(np.allclose(C, gt_C, equal_nan=True))

# def main():
#     from scipy import io as sio
#     mat = sio.loadmat(
#         "/stock_core/test/data/pred_beta1.mat")
#     A = mat['Y']
#     B = mat['predY']
#     gt_C = mat['rg_beta']
#     gt_C = gt_C.reshape(len(gt_C))
#     C = predy_beta(A, B)
#     print(np.allclose(C, gt_C, equal_nan=True))

# def main():
#     from scipy import io as sio
#     mat = sio.loadmat(
#         "/stock_core/test/data/newey_west1.mat")
#     rg_beta = mat['rg_beta']
#     nwbeta, nwse, nwts = neweywest_tstat(rg_beta)
#     nwbeta_gt = mat['nwbeta']
#     nwse_gt = mat['nwse']
#     nwts_gt = mat['nwts']
#     print(np.allclose(nwbeta, nwbeta_gt, equal_nan=True))
#     print(np.allclose(nwse, nwse_gt, equal_nan=True))
#     print(np.allclose(nwts, nwts_gt, equal_nan=True))


# main()

import os
import glob
import ast
from scipy import io as sio
import pandas as pd
from .sys_utils import get_feature_mat_dirs, LOGGER


class SimpleFeature(object):
    def __init__(self, feature_name, df):
        self.feature_name = feature_name
        self.data = df

    def get_raw_data(self):
        '''
        Returns:
            A Pandas.Dataframe contains all data.
        '''
        if self.data is None:
            self.load_data()
        return self.data

    def get_feature_data(self, start_date, end_date):
        '''
        Returns:
            A Pandas.Dataframe contains data between [start_data, end_date].
        '''
        start_date = start_date if isinstance(
            start_date, int) else int(start_date)
        end_date = end_date if isinstance(
            end_date, int) else int(end_date)

        data = self.get_raw_data()
        dates = self.get_dates()

        if start_date < dates[0] or end_date >= dates[-1]:
            LOGGER.warn("The date %lu-%lu is out of range: %lu-%lu",
                        start_date, end_date, dates[0], dates[-1])

        mask = (data.columns >= start_date) & (data.columns < end_date)
        return data.loc[:, mask]

    def get_feature_data_lastday(self, last_day):
        '''
        Returns:
            Return data of last trading date before last_day
        '''
        data = self.get_raw_data()
        last_day = last_day if isinstance(last_day, int) else int(last_day)
        mask = data.columns < last_day
        return data.loc[:, mask].iloc[:, -1:]

    def get_feature_name(self):
        return self.feature_name

    def get_dates(self):
        if self.data is None:
            self.load_data()
        return list(self.data.columns.values)

    def get_stocks(self):
        if self.data is None:
            self.load_data()
        return list(self.data.index.values)

    def load_data(self):
        pass


class StdMatFeat(SimpleFeature):
    """ Standard mat feature.

    The class of standard feature generated from matlab.
    Feature name is the variable name in .mat file.
    Feature shape is (stocks_num, dates_num)
    """

    def __init__(self, feature_name, file_path, stocks, dates):
        SimpleFeature.__init__(self, feature_name, None)
        self.file_path = file_path
        self.stocks = stocks
        self.dates = dates

    def load_data(self):
        mat_data = sio.loadmat(self.file_path, variable_names=[
            self.feature_name])[self.feature_name]
        self.data = pd.DataFrame(
            mat_data, columns=self.dates, index=self.stocks)


class MarketMatFeat(StdMatFeat):
    """ Market mat feature.

    The class of market feature.
    Feature shape is (1, dates_num)
    """

    def __init__(self, feature_name, file_path, dates):
        StdMatFeat.__init__(self, feature_name, file_path, None, dates)

    def get_stocks(self):
        raise NotImplementedError(
            "You can't get stock list from market feature")


class FeatureCenter(object):

    _instance = None

    def __new__(cls, *args, **kw):
        '''Singleton

        Make sure use one feature center instance
        to save memory.
        '''
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kw)
        return cls._instance

    def __init__(self):
        self.features = {}
        self.load_mat_features()

    def load_mat_features(self):
        mat_pathes = get_feature_mat_dirs()
        for mat_path in mat_pathes:
            stock_file = os.path.join(mat_path, 'stocks.txt')
            date_file = os.path.join(mat_path, 'dates.txt')
            with open(stock_file) as f:
                stocks = f.readlines()
                stocks = [x.strip() for x in stocks]
            with open(date_file) as f:
                dates = f.readlines()
                dates = [int(x.strip()) for x in dates]

            mat_files = glob.glob(os.path.join(mat_path, "*.mat"))
            std_shape = (len(stocks), len(dates))
            market_shape = (1, len(dates))
            for mat_file in mat_files:
                features = sio.whosmat(mat_file)
                for feature in features:
                    feature_name = feature[0]
                    if feature[1] == std_shape:
                        self.features[feature_name] = StdMatFeat(
                            feature_name, mat_file, stocks, dates)
                    elif feature[1] == market_shape:
                        self.features[feature_name] = MarketMatFeat(
                            feature_name, mat_file, dates)

    def get_feature_list(self):
        return self.features.keys()

    def get_feature(self, feature_name):
        if feature_name not in self.features:
            raise KeyError("Feature:%s doesn't existed!" % feature_name)
        else:
            return self.features[feature_name]

    def get_expr_feature(self, expr):
        expr = expr.strip()
        if expr in self.features:
            return self.features[expr]

        root = ast.parse(expr)
        v_names = {node.id for node in ast.walk(
            root) if isinstance(node, ast.Name)}

        get_cmd = "%s = self.get_feature('%s').get_raw_data().to_numpy()"
        for v_name in v_names:
            exec(get_cmd % (v_name, v_name), locals())

        result = eval(expr, locals())

        check_cmd = "result.shape == %s.shape"
        stocks_cmd = "self.get_feature('%s').get_stocks()"
        dates_cmd = "self.get_feature('%s').get_dates()"
        for v_name in v_names:
            if eval(check_cmd % v_name, locals()):
                result_stocks = eval(stocks_cmd % v_name, locals())
                result_dates = eval(dates_cmd % v_name, locals())
                break
        df = pd.DataFrame(
            result, columns=result_dates, index=result_stocks)
        expr_feat = SimpleFeature(expr, df)
        self.features[expr] = expr_feat
        return expr_feat


# def main():
#     f = FeatureCenter()

#     a = f.get_expr_feature("M_fret1d - hdg_fret1d")
#     b = f.get_feature("M_fret1d")
#     c = f.get_feature("hdg_fret1d")
#     da = a.get_feature_data(20100202, 20110202).to_numpy()
#     db = b.get_feature_data(20100202, 20110202).to_numpy()
#     dc = c.get_feature_data(20100202, 20110202).to_numpy()
#     dd = db - dc
#     print(da)
#     print(dd)
#     import numpy as np
#     print(((da == dd) | (np.isnan(da) & np.isnan(dd))).all())

# main()

from .utils import decorr, pack_yx, ModelInput, ModelOutput
from .sys_utils import LOGGER
import numpy as np


def decorr_plugin(model_inputs, f_train, f_test, step=63):
    f_train = f_train.to_numpy()
    f_test = f_test.to_numpy()
    for model_input in model_inputs:
        model_input.train_y = decorr(model_input.train_y, f_train, step)
        model_input.test_y = decorr(model_input.test_y, f_test, step)
    return model_inputs


def get_preprocs_plugin(plugin_name):
    if plugin_name == 'decorr':
        return decorr_plugin
    else:
        raise TypeError("Unknow plugin type: %s" % plugin_name)


class ModelRunner(object):

    def __init__(self, train_y_df, train_x_dfs, test_y_df, test_x_dfs, models, model_configs):
        self.raw_input = ModelInput(
            *(pack_yx(train_y_df, *train_x_dfs) + pack_yx(test_y_df, *test_x_dfs)))
        self.inputs = [self.raw_input]
        self.models = models
        self.model_configs = model_configs
        self.res = {}

    def execute_preprocs(self, plugin_name, args=[]):
        self._execute_preprocs(get_preprocs_plugin(plugin_name), args)
        return self

    def _execute_preprocs(self, plugin, args=[]):
        args = [self.inputs] + list(args)
        self.inputs = plugin(*args)
        return self

    def run(self):
        self.res.clear()
        for idx, model in enumerate(self.models):
            model_name = model.__name__
            model_config = self.model_configs[idx]
            if len(self.inputs) == 1:
                self.res[model_name] = self.run_combine(model, model_config)
            else:
                self.res[model_name] = self.run_partition(model, model_config)

    def run_partition(self, model, model_config):
        pass

    def run_combine(self, model, model_config):
        _input = self.inputs[0]
        execute_res = model.execute(
            _input.train_y, _input.train_x, _input.test_x, model_config)
        train_pred_y = execute_res[0]
        test_pred_y = execute_res[1]
        user_data = None
        if len(execute_res) == 3:
            user_data = execute_res[2]
        return ModelOutput(_input.train_y,
                           train_pred_y,
                           _input.test_y,
                           test_pred_y,
                           user_data)

    def get_raw_inputs(self):
        return self.raw_input

    def get_result(self):
        return self.res

    def get_combined_result(self):
        outputs = list(self.res.values())
        if len(outputs) == 0:
            LOGGER.warn('Plz run models before getting result!')
            return None
        train_pred_y_sum = np.zeros_like(outputs[0].train_pred_y)
        test_pred_y_sum = np.zeros_like(outputs[0].test_pred_y)
        user_data_sum = {}
        for idx, output in enumerate(outputs):
            train_pred_y_sum += output.train_pred_y
            test_pred_y_sum += output.test_pred_y
            if output.user_data is not None:
                user_data_sum[self.models[idx].__name__] = output.user_data
        return ModelOutput(outputs[0].train_y,
                           train_pred_y_sum,
                           outputs[0].test_y,
                           test_pred_y_sum,
                           user_data_sum)

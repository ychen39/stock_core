import numpy as np
from scipy.stats.mstats import winsorize
from .utils import general_evaluate, corr_per_day


def demean_plugin(model_output):
    model_output.test_pred_y -= np.mean(model_output.test_pred_y, axis=0)
    return model_output


def long_only_plugin(model_output):
    model_output.test_pred_y[model_output.test_pred_y < 0] = 0
    return model_output


def get_postprocs_plugin(plugin_name):
    if plugin_name == 'demean':
        return demean_plugin
    elif plugin_name == 'long_only_plugin':
        return long_only_plugin
    else:
        raise TypeError("Unknow plugin type: %s" % plugin_name)


def default_evaluate(model_output, metrics):
    train_y = model_output.train_y
    train_pred_y = model_output.train_pred_y
    test_y = model_output.test_y
    test_pred_y = model_output.test_pred_y

    metrics.update(general_evaluate(test_pred_y, test_y))

    rho_iu = np.mean(corr_per_day(train_pred_y, train_y, 'Pearson'))

    metrics['rho_iu'] = rho_iu
    metrics['nstocks'] = len(train_y)
    return metrics


class Evaluator(object):
    def __init__(self, model_output):
        self.model_output = model_output
        self.metrics = {}

    def execute_postprocs(self, plugin_name, args=[]):
        self._execute_postprocs(get_postprocs_plugin(plugin_name), args)
        return self

    def _execute_postprocs(self, plugin, args=[]):
        args = [self.model_output] + list(args)
        self.model_output = plugin(*args)
        return self

    def evaluate(self, fuc=default_evaluate, args=[]):
        args = [self.model_output, self.metrics] + list(args)
        self.metrics = fuc(*args)
        return self

    def get_metrics(self):
        return self.metrics

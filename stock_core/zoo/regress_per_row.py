import numpy as np
from .base import Model


class RegressPerRow(Model):

    def execute(train_y, train_x, test_x, model_config):
        iter_num = len(train_y)
        x_row_per_iter = int(len(train_x) / iter_num)
        x_indexes = np.zeros((x_row_per_iter,), dtype=int)
        for i in range(1, x_row_per_iter):
            x_indexes[i] = x_indexes[i-1] + iter_num

        train_pred_y = np.zeros_like(train_y)
        test_pred_y = np.zeros((iter_num, test_x.shape[1]))

        for i in range(0, iter_num):
            t_x = train_x[x_indexes]
            tb = np.linalg.lstsq(t_x.T, train_y[i].T, rcond=None)[0]
            train_pred_y[i] = (t_x.T @ tb).T
            test_pred_y[i] = (test_x[x_indexes].T @ tb).T
            x_indexes += 1

        np.nan_to_num(test_pred_y, nan=0, copy=False)
        test_pred_y[np.isinf(test_pred_y)] = 0
        return train_pred_y, test_pred_y


# def main():
#     from scipy import io as sio
#     mat = sio.loadmat(
#         "/stock_core/test/data/regress_per_row1.mat")
#     train_y = mat['Y_train']
#     train_x = mat['X_train']
#     test_x = mat['X_test']

#     pred_y, pred_y_test = RegressPerRow.execute(train_y, train_x, test_x)

#     gt_pred_y = mat['predY']
#     gt_pred_y_test = mat['predY_test']
#     print(np.allclose(gt_pred_y, pred_y, equal_nan=True))
#     print(np.allclose(gt_pred_y_test, pred_y_test, equal_nan=True))


# main()

from abc import ABCMeta, abstractmethod


class Model(metaclass=ABCMeta):
    @abstractmethod
    def execute(train_y, train_x, test_x, model_config):
        pass

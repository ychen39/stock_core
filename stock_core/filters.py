import numpy as np
import pandas as pd
from .sys_utils import BLACK_STOCKS_HEAD


def dropna_filter(root_mask, *args):
    '''Dropna filter

    Remove stocks which have all nan values.
    '''
    dfs = list(args)

    for df in dfs:
        root_mask = root_mask & ~(df.isna().any(
            axis=1)) & ~((df == 0).all(axis=1))
    return root_mask


def BL_filter(root_mask, stocks):
    '''Black list filter

    Remove stocks which are in black list.
    '''
    stocks = pd.Series(stocks)
    root_mask = root_mask & ~(
        stocks.str.startswith(BLACK_STOCKS_HEAD).to_numpy())
    return root_mask


def rank_filter(root_mask, field, op='max', k=3000):
    '''Rank filter

    Only keep stocks which are max/min k values in field
    '''
    field = field.to_numpy().flatten()
    if op == 'max':
        temp = (-field).argsort()
    else:
        temp = field.argsort()
    ranks = np.empty_like(temp)
    ranks[temp] = np.arange(1, len(field)+1)
    mask = ranks <= k
    return mask & root_mask


def get_filter(filter_name):
    if filter_name == "dropnan":
        return dropna_filter
    elif filter_name == "rank":
        return rank_filter
    elif filter_name == "BL":
        return BL_filter
    else:
        raise TypeError("Unknow filter type: %s" % filter_name)


class StockFilterWrapper():

    def __init__(self, stocks):
        self.mask = np.ones(len(stocks), dtype=bool)
        self.stocks = stocks

    def update_mask(self, filter_name, args=[]):
        self._update_mask(get_filter(filter_name), args)
        return self

    def _update_mask(self, filter, args=[]):
        args = [self.mask] + list(args)
        self.mask = filter(*args)
        return self

    def filter(self, *dfs):
        res = []
        for df in dfs:
            res.append(df[self.mask])
        if len(res) == 1:
            return res[0]
        else:
            return res

    def get_stocks(self):
        return np.asarray(self.stocks)[self.mask].tolist()

    def get_mask(self):
        return self.mask


# def main():
#     import features
#     f = features.FeatureCenter()

#     a = f.get_expr_feature("M_fret1d - hdg_fret1d")
#     b = f.get_feature("M_fret1d")
#     a_df = a.get_feature_data(20180202, 20190204)
#     b_df = b.get_feature_data(20180202, 20190204)

#     sadv = f.get_feature("M_sadv1y").get_feature_data(
#         20190204, 20190204)

#     stocks = a.get_stocks()
#     sf = StockFilterWrapper(stocks)
#     sf.update_mask('dropnan', (a_df, b_df))
#     sf.update_mask('BL', (stocks, ))

#     print(a_df.shape, b_df.shape)
#     a_df2, b_df2 = sf.filter(a_df, b_df)
#     print(a_df2.shape, b_df2.shape)

#     sf.update_mask('rank', (sadv, 'max', 30))
#     a_df3, b_df3 = sf.filter(a_df, b_df)
#     print(a_df3, b_df3)


# main()

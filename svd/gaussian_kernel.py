import os
import uuid
import numpy as np
from scipy.sparse.linalg import LinearOperator
from numpy.lib.format import open_memmap
from numba import jit, prange
from pycuda import gpuarray
from base_kernel import Kernel
from svd_config import calculate_block_size
from gaussian_kernel_cu import k_gpu, k_gpu_standalone


def gaussian(gamma, x, y):
    return np.exp(-gamma * np.sum((x - y) ** 2, 1))


class MiniGaussianKernel(Kernel):
    def __init__(self, x, y, config):
        self.gamma = config['gamma']

        n = len(x)
        m = len(y)
        self.data = np.zeros((n, m))

        # K(:,j) = exp(-obj.gamma*sum((X - Y(j,:)).^2,2))
        # for j in range(0, m):
        #     self.data[:, j] = gaussian(self.gamma, x, y[j, :])

        for j in range(0, n):
            self.data[j, :] = gaussian(self.gamma, x[j, :], y)

    def get_row(self, row_idx):
        return self.data[row_idx, :]

    def get_col(self, col_idx):
        return self.data[:, col_idx]

    def get_tile(self, row_begin_idx, row_end_idx, col_begin_idx, col_end_idx):
        return self.data[row_begin_idx:row_end_idx, col_begin_idx:col_end_idx]

    def raw_data(self):
        return self.data

    def save(self, path):
        f_name = "gaussian-" + str(uuid.uuid4()) + '.npy'
        f_path = os.path.join(path, f_name)
        np.save(f_path, self.data)
        return f_name


@jit(parallel=True, nopython=True)
def _get_tile(gamma, x, y, row_begin_idx, row_end_idx, col_begin_idx, col_end_idx):
    assert(row_end_idx > row_begin_idx)
    assert(col_end_idx > col_begin_idx)
    n = row_end_idx - row_begin_idx
    m = col_end_idx - col_begin_idx
    data = np.zeros((n, m))
    for j in prange(row_begin_idx, row_end_idx):
        data[j - row_begin_idx, :] = np.exp(-gamma * np.sum(
            (x[j, :] - y[col_begin_idx:col_end_idx]) ** 2, 1))
    return data


@jit(parallel=True, nopython=True)
def _mat_mul(gamma, x, y, mat, block_sz):
    n = len(x)
    m = mat.shape[1]
    assert(len(y) == mat.shape[0])
    res = np.zeros((n, m))

    if n < block_sz:
        block_sz = n
    cache = np.zeros((block_sz, len(y)))
    for i in range(0, n // block_sz * block_sz, block_sz):
        for j in prange(i, i + block_sz):
            cache[j - i] = np.exp(-gamma * np.sum((x[j, :] - y[:]) ** 2, 1))
        res[i:i + block_sz] = cache @ mat
    tail = n % block_sz
    if tail != 0:
        for j in prange(n - tail, n):
            cache[j - n + tail] = np.exp(-gamma *
                                         np.sum((x[j, :] - y[:]) ** 2, 1))
        tmp = cache[:tail] @ mat
        res[n - tail:] = tmp

    # for i in prange(0, n):
    #     for j in prange(0, m):
    #         tmp = np.exp(-gamma * np.sum((x[i, :] - y[:]) ** 2, 1)) @ mat[:, j]
    #         res[i, j] = tmp
    return res


def _mat_mul_gpu(gamma, x, y, v, block_sz):
    # return k_gpu_standalone(x, y, mat, gamma)
    n = len(x)
    m = len(y)
    d = x.shape[1]

    assert(m == v.shape[0])
    assert(d == y.shape[1])

    if n < block_sz:
        block_sz = n
    cache_d = gpuarray.empty((block_sz, m), np.float64)
    v_d = gpuarray.zeros(v.shape, np.float64)
    for i in range(v.shape[1]):
        v_d[:, i] = v[:, i]
    # if v.strides != cache_d.strides:
    #     tmp = np.zeros(v.shape)
    #     tmp[:] = v[:]
    #     v = tmp

    y_d = gpuarray.to_gpu(y.flatten())
    # v_d = gpuarray.to_gpu(v)
    res = np.zeros((n, v.shape[1]))

    for i in range(0, n // block_sz * block_sz, block_sz):
        x_d = gpuarray.to_gpu(x[i:i + block_sz].flatten())
        res[i:i + block_sz] = k_gpu(x_d, y_d, v_d,
                                    cache_d, gamma, block_sz, m, d).get()

    tail = n % block_sz
    if tail != 0:
        x_d = gpuarray.to_gpu(x[n - tail:n].flatten())
        res[n - tail:] = k_gpu(x_d, y_d, v_d,
                               cache_d, gamma, tail, m, d).get()[:tail]
    return res


class GaussianKernel(Kernel):
    def __init__(self, x, y, config, gpu=True):
        self.gamma = config['gamma']

        self.x = x
        self.y = y
        self.n = len(x)
        self.m = len(y)

        block_sz = calculate_block_size(self.m, 8)  # double

        def matmul(v):
            if len(v.shape) == 1:
                v = v.reshape((len(v), 1))
            if gpu:
                return _mat_mul_gpu(self.gamma, x, y, v, block_sz)
            else:
                return _mat_mul(self.gamma, x, y, v, block_sz)
        self.data = LinearOperator(
            (self.n, self.m), matvec=matmul, matmat=matmul)

    def get_row(self, row_idx):
        return self._get_row(row_idx)

    def get_col(self, col_idx):
        return self._get_col(col_idx)

    def get_tile(self, row_begin_idx, row_end_idx, col_begin_idx, col_end_idx):
        return _get_tile(self.gamma,
                         self.x,
                         self.y,
                         row_begin_idx,
                         row_end_idx,
                         col_begin_idx,
                         col_end_idx)

    def _get_row(self, row_idx, col_range=slice(None)):
        return gaussian(self.gamma, self.x[row_idx, :], self.y[col_range])

    def _get_col(self, col_idx, row_range=slice(None)):
        return gaussian(self.gamma, self.x[row_range], self.y[col_idx, :])

    def raw_data(self):
        # data = np.zeros((self.n, self.m))
        # for j in range(0, self.n):
        #     data[j, :] = gaussian(self.gamma, self.x[j, :], self.y)
        # return data
        return self.data

    def save(self, path):
        f_name = "gaussian-" + str(uuid.uuid4()) + '.npy'
        f_path = os.path.join(path, f_name)
        data = open_memmap(f_path, mode='w+',
                           dtype=self.x.dtype, shape=((self.n, self.m)))

        for j in range(0, self.n):
            self.data[j, :] = gaussian(self.gamma, self.x[j, :], self.y)

        # for j in range(0, self.m):
        #     data[:, j] = gaussian(self.gamma, self.x, self.y[j, :])
        data.flush()
        return f_name


# def main():
#     import primme
#     import scipy.sparse
#     import numpy as np
#     size = 8000

#     x = np.random.rand(size, 2)
#     vec = np.random.rand(size, 20)
#     k = GaussianKernel(x, x, {'gamma': 0.1}).raw_data()
#     k2 = MiniGaussianKernel(x, x, {'gamma': 0.1}).raw_data()
#     k3 = GaussianKernel(x, x, {'gamma': 0.1}, gpu=False).raw_data()

#     res1 = k @ vec
#     res2 = k2 @ vec
#     res3 = k3 @ vec
#     print('test1:', np.allclose(res1, res2, equal_nan=True))
#     print('test2:', np.allclose(res1, res3, equal_nan=True))
#     from low_rank import low_rank_mini, low_rank_primme
#     from time import time
#     start = time()
#     evals0, evecs0 = low_rank_mini(k2)

#     print(evecs0[:30])
#     print(time() - start)
#     print('======')
#     start = time()
#     evals1, evecs1 = low_rank_primme(k3)

#     print(evecs1[:30])
#     print(time() - start)
#     print('======')
#     start = time()
#     evals2, evecs2 = low_rank_primme(k)

#     print(evecs2[:30])
#     print(time() - start)

#     a = np.sum(evecs1 ** 2)
#     b = np.sum(evecs2 ** 2)
#     c = np.sum((evecs1 - evecs2) ** 2)

#     print('test3:', c / a, c / b)


# main()



import os
import math
import uuid
import itertools
import heapq
import sys
from time import time
import datetime
import numpy as np
from scipy import stats
from skopt import Optimizer
from numpy.random import default_rng
from stock_core.zoo import base
from stock_core.api import engine, date_config
from stock_core.zoo import regress_per_row
from stock_core import sys_utils, evaluator
from stock_core.utils import corr_per_day
from kernel_builder import get_kernel
from low_rank import low_rank

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def zscore_features(train_x, test_x):
    sigma = np.std(train_x, axis=1, ddof=1)
    train_x = stats.zscore(train_x, axis=1, ddof=1)
    np.nan_to_num(train_x, copy=False)
    test_x = test_x * (1 / sigma)[:, np.newaxis]
    test_x[sigma == 0] = 0

    sigma_cap = 3
    train_x[train_x > sigma_cap] = sigma_cap
    train_x[train_x < -sigma_cap] = -sigma_cap
    test_x[test_x > sigma_cap] = sigma_cap
    test_x[test_x < -sigma_cap] = -sigma_cap
    return train_x, test_x


# def calculate_alpha(evecs, evals, lambda_value, rank, train_y, y1):
#     # y1 = evecs.T @ train_y
#     y2 = np.diag((1 / (evals + lambda_value)).flatten()) @ y1
#     return evecs[:, 0:rank] @ y2[0:rank]


class KernelRidgeLowRank(base.Model):
    def execute(train_y, train_x, test_x, model_config):
        tr_x_shape = train_x.shape
        tr_y_shape = train_y.shape
        te_x_shape = test_x.shape
        te_y_shape = (tr_y_shape[0], te_x_shape[1])

        train_x, test_x = zscore_features(train_x, test_x)

        train_y = np.reshape(train_y, (tr_y_shape[0] * tr_y_shape[1], 1))
        cancat_train_x = []
        cancat_test_x = []
        for i in range(0, tr_x_shape[0], tr_y_shape[0]):
            cancat_train_x.append(
                train_x[i:i + tr_y_shape[0]].reshape(train_y.shape))
            cancat_test_x.append(
                test_x[i:i + tr_y_shape[0]].reshape(tr_y_shape[0] * te_x_shape[1], 1))
        train_x = np.concatenate(cancat_train_x, axis=1)
        test_x = np.concatenate(cancat_test_x, axis=1)

        ntrain = len(train_y)
        cache_file = model_config['cache_file']
        k_type = model_config['kernel_type']
        comp_cache = model_config['computation_cache']

        _gamma = model_config['gamma']
        _lamda = model_config['lamda']
        _rank = model_config['rank']

        # print('start build kernel, problem size:', ntrain, flush=True)
        start_time = time()
        k = get_kernel(train_x, train_x, k_type,
                       {'gamma': _gamma}).raw_data()
        k_test = get_kernel(test_x, train_x, k_type,
                            {'gamma': _gamma}).raw_data()
        # print('end build kernel, time:', '%.2f' %
        #       (time() - start_time), flush=True)

        if 'evecs' in comp_cache:
            # print('load evecs from cache', flush=True)
            evecs = comp_cache['evecs']
            evals = comp_cache['evals']
            y1 = comp_cache['y1']
            alphas = comp_cache['alphas']
        else:
            if os.path.exists(cache_file):
                # print('load evecs from file:', cache_file, flush=True)
                data = np.load(cache_file)
                evecs = data['evecs']
                evals = data['evals']
                y1 = data['y1']
            else:
                # print('start solve svd, problem size:', ntrain, flush=True)
                start_time = time()
                evecs, evals = low_rank(k)
                y1 = evecs.T @ train_y
                # print('end solve svd, time:', '%.2f' %
                #       (time() - start_time), flush=True)
                np.savez(cache_file, evecs=evecs,
                         evals=evals, y1=y1, train_x=train_x)

            alphas = {}
            comp_cache['evecs'] = evecs
            comp_cache['evals'] = evals
            comp_cache['y1'] = y1
            comp_cache['alphas'] = alphas

        # print('start calculate y2', flush=True)
        start_time = time()
        y2 = np.diag((1 / (evals + _lamda)).flatten()) @ y1
        # print('end calculate y2, time:', '%.2f' %
        #   (time() - start_time), flush = True)

        # print('start calculate alpha', flush=True)
        start_time = time()
        start_rank = 0
        if _lamda in alphas:
            prev_alpha, prev_rank = alphas[_lamda]
            if _rank >= prev_rank:
                start_rank = prev_rank

        alpha = evecs[:, start_rank:_rank] @ y2[start_rank:_rank]
        if start_rank != 0:
            alpha += prev_alpha
        alphas[_lamda] = (alpha, _rank)
        # print('end calculate alpha, time:', '%.2f' %
        #       (time() - start_time), flush=True)

        # alpha = calculate_alpha(
        #     evecs, evals, _lamda, _rank, train_y, y1)

        # print('start forecast', flush=True)
        start = time()
        train_predy_y = k @ alpha
        test_pred_y = k_test @ alpha
        # print('end forecast, time:', '%.2f' %
        #       (time() - start_time), flush=True)

        train_pred_y = np.reshape(train_predy_y, tr_y_shape)
        test_pred_y = np.reshape(test_pred_y, te_y_shape)

        test_pred_y = test_pred_y / np.std(test_pred_y, axis=0, ddof=1)
        return train_pred_y, test_pred_y, {'ntrain': ntrain, 'min_eigv': evals[_rank - 1]}


def exectue(_gamma,
            te_st_dt,
            hyper_para_rank,
            hyper_para_lambda,
            hyper_para_kernel,
            engine,
            cache_folder,
            dt_res_folder):
    comp_cache = {}
    cache_f_name = str(te_st_dt) + "_" + \
        str(_gamma).replace('.', '_') + ".npz"
    cache_f_name = os.path.join(cache_folder, cache_f_name)

    best_corr = -1
    best_metrics = None
    best_para = None
    best_model_output = None

    for _rank, _lambda, _kernel in list(itertools.product(
            hyper_para_rank,
            hyper_para_lambda,
            hyper_para_kernel)):
        # print(te_st_dt, 'gamma:', _gamma,
        #       'lamda', _lambda, 'rank', _rank, flush=True)
        start_time = time()
        model_output, metrics = engine.run_models([{
            'gamma': _gamma,
            'lamda': _lambda,
            'rank': _rank,
            'kernel_type': _kernel,
            'computation_cache': comp_cache,
            'cache_file': cache_f_name,
        }])
        # print('total time:', '%.2f' %
        #       (time() - start_time), flush=True)
        n_train = model_output.user_data['KernelRidgeLowRank']['ntrain']
        min_eigv = model_output.user_data['KernelRidgeLowRank']['min_eigv']

        paras = {'gamma': _gamma,
                 'lamda': _lambda,
                 'rank': _rank,
                 'ntrain': n_train,
                 'min_eigv': min_eigv}

        if metrics['rho_u'] > best_corr:
            best_corr = metrics['rho_u']
            best_metrics = metrics
            best_para = paras
            best_model_output = model_output

        res_f_name = str(uuid.uuid4()) + ".npz"
        res_f_name = os.path.join(dt_res_folder, res_f_name)
        np.savez(res_f_name, **metrics, **paras)

        # print("*" * 10, flush=True)
        # sys_utils.print_metrics(te_st_dt, metrics, paras)
    return best_corr, best_metrics, best_para, best_model_output


def evaluate_boosting_res(model_list, res_folder):
    f_name = "y.npz"
    f_name = os.path.join(res_folder, f_name)
    np.savez(f_name, train_y=model_list[0]
             [1].train_y, test_y=model_list[0][1].test_y)

    for m in model_list:
        f_name = str(uuid.uuid4()) + ".npz"
        f_name = os.path.join(res_folder, f_name)
        np.savez(f_name, train_pred_y=m[1].train_pred_y,
                 test_pred_y=m[1].test_pred_y, **(m[2]))

    glued_output = model_list[0][1]
    for i in range(1, len(model_list)):
        glued_output.train_pred_y += model_list[i][1].train_pred_y
        glued_output.test_pred_y += model_list[i][1].test_pred_y
    glued_output.train_pred_y /= len(model_list)
    glued_output.test_pred_y /= len(model_list)

    t = evaluator.Evaluator(glued_output)
    m = t.evaluate().get_metrics()
    f_name = "boosting.npz"
    f_name = os.path.join(res_folder, f_name)
    np.savez(f_name, **m)

    return m


def main():
    # features = ['last_day_2pmto4pm_return',
    #             'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    # train_response = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    features = ['last_day_2pmto4pm_return', 'sadv_past_63d']
    train_response = 'today_open_to_close_return - today_open_to_close_return[18596]'
    test_response = train_response
    universe = 50

    if len(sys.argv) == 1:
        sadv_y = 1
    else:
        sadv_y = int(sys.argv[1])

    # Setup engine
    e = engine.Engine()
    e.features(features)
    e.responses(train_response, test_response)
    e.universe('sadv_past_%dy' % sadv_y, 'max', universe)
    e.models([KernelRidgeLowRank])
    # e.decorr_y('sadv_past63_day').postprocs('demean')

    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)

    # hyper_para_gamma = [10, 5, 1, 0.1, 0.01]
    opt_iter = 100
    hyper_para_rank = list(range(1, 25))
    hyper_para_lambda = [0]
    hyper_para_kernel = [0]
    boosting_model_num = 10

    # cache_folder = os.path.join(os.path.dirname(
    #    os.path.abspath(__file__)), str(sadv_y) + '_opt_cache_' + str(universe))
    cache_folder = os.path.join(
        "/scratch/ychen/svd_cache", str(sadv_y) + '_opt_cache_' + str(universe))
    print("cache folder:", cache_folder)
    os.makedirs(cache_folder, exist_ok=True)

    res_folder = os.path.join(os.path.dirname(os.path.abspath(
        __file__)), str(sadv_y) + '_opt_res_' + str(universe) + '_' + datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
    os.makedirs(res_folder, exist_ok=True)

    boosting_res_folder = os.path.join(res_folder, 'boosting')
    os.makedirs(boosting_res_folder, exist_ok=True)

    best_start_dates = []
    best_res = []
    best_para_info = []
    boosting_res = []

    for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in d_f:
        # Setup date for a time range
        e.prepare_data(tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt)
        best_corr = -1
        best_metrics = None
        best_para = None

        test_start_dates = []
        res = []
        para_info = []

        dt_res_folder = os.path.join(res_folder, str(te_st_dt))
        os.makedirs(dt_res_folder, exist_ok=True)

        dt_boosting_res_folder = os.path.join(
            boosting_res_folder, str(te_st_dt))
        os.makedirs(dt_boosting_res_folder, exist_ok=True)

        opt = Optimizer([(-2.0, 2.0)])
        iter_idx = 0
        gamma_to_corr = {}

        max_heap = []
        while iter_idx < opt_iter:
            suggested = opt.ask()
            _gamma = math.pow(10, suggested[0])
            if _gamma in gamma_to_corr:
                corr = gamma_to_corr[_gamma]
                opt_res = opt.tell(suggested, -corr)
                print(te_st_dt, 'step:', iter_idx, 'gamma:',
                      _gamma, 'corr:', corr, flush=True)
                iter_idx += 1
                continue

            corr, metrics, paras, model_output = exectue(_gamma,
                                                         te_st_dt,
                                                         hyper_para_rank,
                                                         hyper_para_lambda,
                                                         hyper_para_kernel,
                                                         e,
                                                         cache_folder,
                                                         dt_res_folder)

            print(te_st_dt, 'step:', iter_idx, 'gamma:',
                  _gamma, 'corr:', corr, flush=True)

            if metrics['rho_u'] > best_corr:
                best_corr = corr
                best_metrics = metrics
                best_para = paras

            test_start_dates.append(te_st_dt)
            res.append(metrics)
            para_info.append(paras)

            opt_res = opt.tell(suggested, -corr)
            iter_idx += 1
            gamma_to_corr[_gamma] = corr

            # Boosting
            heapq.heappush(max_heap, (corr, model_output, metrics))
            if len(max_heap) > boosting_model_num:
                heapq.heappop(max_heap)

        print(te_st_dt, "end")
        print("*" * 10)

        best_start_dates.append(te_st_dt)
        best_res.append(best_metrics)
        best_para_info.append(best_para)

        boosting_res.append(evaluate_boosting_res(
            max_heap, dt_boosting_res_folder))

        sys_utils.print_metrics_list(test_start_dates, res, para_info)

    sys_utils.print_metrics_list(best_start_dates, best_res, best_para_info)
    sys_utils.print_metrics_list(best_start_dates, boosting_res)


main()


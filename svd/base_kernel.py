from abc import ABCMeta, abstractmethod


class Kernel(metaclass=ABCMeta):

    @abstractmethod
    def get_row(self, row_idx):
        pass

    @abstractmethod
    def get_col(self, col_idx):
        pass

    @abstractmethod
    def get_tile(self, row_begin_idx, row_end_idx, col_begin_idx, col_end_idx):
        pass

    @abstractmethod
    def raw_data(self):
        pass

    @abstractmethod
    def save(self, path):
        pass

import os
import uuid
import itertools
from time import time
import datetime
import numpy as np
from scipy import stats
from numpy.random import default_rng
from stock_core.zoo import base
from stock_core.api import engine, date_config
from stock_core.zoo import regress_per_row
from stock_core import sys_utils
from stock_core.utils import corr_per_day
from kernel_builder import get_kernel
from low_rank import low_rank

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def zscore_features(train_x, test_x):
    sigma = np.std(train_x, axis=1, ddof=1)
    train_x = stats.zscore(train_x, axis=1, ddof=1)
    np.nan_to_num(train_x, copy=False)
    test_x = test_x * (1 / sigma)[:, np.newaxis]
    test_x[sigma == 0] = 0

    sigma_cap = 3
    train_x[train_x > sigma_cap] = sigma_cap
    train_x[train_x < -sigma_cap] = -sigma_cap
    test_x[test_x > sigma_cap] = sigma_cap
    test_x[test_x < -sigma_cap] = -sigma_cap
    return train_x, test_x


# def calculate_alpha(evecs, evals, lambda_value, rank, train_y, y1):
#     # y1 = evecs.T @ train_y
#     y2 = np.diag((1 / (evals + lambda_value)).flatten()) @ y1
#     return evecs[:, 0:rank] @ y2[0:rank]


class KernelRidgeLowRankPerRow(base.Model):
    def execute(train_y, train_x, test_x, model_config):
        train_x, test_x = zscore_features(train_x, test_x)

        stock_num = len(train_y)
        feat_num = len(train_x) // stock_num

        k_type = model_config['kernel_type']
        _gamma = model_config['gamma']
        _lamda = model_config['lamda']
        _rank = model_config['rank']
        comp_cache = model_config['computation_cache']
        if 'evecs' in comp_cache:
            cached = True
            evecs_list = comp_cache['evecs']
            evals_list = comp_cache['evals']
            y1_list = comp_cache['y1']
            # alpha_list = comp_cache['alphas']
        else:
            cached = False
            evecs_list = []
            evals_list = []
            y1_list = []
            # alpha_list = []

        cancat_pred_train_y = []
        cancat_pred_test_y = []
        for id in range(stock_num):
            cancat_train_x = []
            cancat_test_x = []
            for feat_id in range(feat_num):
                cancat_train_x.append(train_x[feat_id * stock_num + id])
                cancat_test_x.append(test_x[feat_id * stock_num + id])
            stock_train_x = np.vstack(cancat_train_x).T
            stock_test_x = np.vstack(cancat_test_x).T
            stock_train_y = train_y[id]

            k = get_kernel(stock_train_x, stock_train_x, k_type,
                           {'gamma': _gamma}).raw_data()
            k_test = get_kernel(stock_test_x, stock_train_x, k_type,
                                {'gamma': _gamma}).raw_data()

            if cached:
                evecs = evecs_list[id]
                evals = evals_list[id]
                y1 = y1_list[id]
            else:
                evecs, evals = low_rank(k)
                y1 = evecs.T @ stock_train_y

                evecs_list.append(evecs)
                evals_list.append(evals)
                y1_list.append(y1)

            y2 = np.diag((1 / (evals + _lamda)).flatten()) @ y1
            alpha = evecs[:, 0:_rank] @ y2[0:_rank]

            cancat_pred_train_y.append(k @ alpha)
            cancat_pred_test_y.append(k_test @ alpha)

        if cached == False:
            comp_cache['evecs'] = evecs_list
            comp_cache['evals'] = evals_list
            comp_cache['y1'] = y1_list
            # comp_cache['alphas'] = alpha_list

        train_pred_y = np.vstack(cancat_pred_train_y)
        test_pred_y = np.vstack(cancat_pred_test_y)

        test_pred_y = test_pred_y / np.std(test_pred_y, axis=0, ddof=1)
        return train_pred_y, test_pred_y


def main():
    # features = ['last_day_2pmto4pm_return',
    #             'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    # train_response = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    features = ['last_day_2pmto4pm_return', 'sadv_past_63d']
    train_response = 'today_open_to_close_return - today_open_to_close_return[18596]'
    test_response = train_response
    universe = 50
    sadv_y = 3

    # Setup engine
    e = engine.Engine()
    e.features(features)
    e.responses(train_response, test_response)
    e.universe('sadv_past_%dy' % sadv_y, 'max', universe)
    e.models([KernelRidgeLowRankPerRow])
    # e.decorr_y('sadv_past63_day').postprocs('demean')

    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)

    hyper_para_gamma = [10, 5, 1, 0.1, 0.01]
    hyper_para_rank = list(range(1, 26))
    hyper_para_lambda = [0]
    hyper_para_kernel = [0]

    res_folder = os.path.join(os.path.dirname(os.path.abspath(
        __file__)), str(sadv_y) + '_res_perrow_' + str(universe) + '_' + datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
    os.makedirs(res_folder, exist_ok=True)

    test_start_dates = []
    res = []
    para_info = []

    best_start_dates = []
    best_res = []
    best_para_info = []

    for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in d_f:
        # Setup date for a time range
        e.prepare_data(tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt)
        best_corr = -1
        best_metrics = None
        best_para = None

        dt_res_folder = os.path.join(res_folder, str(te_st_dt))
        os.makedirs(dt_res_folder, exist_ok=True)

        for _gamma in hyper_para_gamma:
            comp_cache = {}
            for _rank, _lambda, _kernel in list(itertools.product(
                    hyper_para_rank,
                    hyper_para_lambda,
                    hyper_para_kernel)):
                print(te_st_dt, 'gamma:', _gamma,
                      'lamda', _lambda, 'rank', _rank, flush=True)
                start_time = time()
                model_output, metrics = e.run_models([{
                    'gamma': _gamma,
                    'lamda': _lambda,
                    'rank': _rank,
                    'kernel_type': _kernel,
                    'computation_cache': comp_cache
                }])
                print('total time:', '%.2f' %
                      (time() - start_time), flush=True)

                test_start_dates.append(te_st_dt)
                res.append(metrics)
                paras = {'gamma': _gamma,
                         'lamda': _lambda,
                         'rank': _rank
                         }

                if metrics['rho_u'] > best_corr:
                    best_corr = metrics['rho_u']
                    best_metrics = metrics
                    best_para = paras

                res_f_name = str(uuid.uuid4()) + ".npz"
                res_f_name = os.path.join(dt_res_folder, res_f_name)
                np.savez(res_f_name, **metrics, **paras)

                para_info.append(paras)
                sys_utils.print_metrics(te_st_dt, metrics, paras)
        best_start_dates.append(te_st_dt)
        best_res.append(best_metrics)
        best_para_info.append(best_para)

    sys_utils.print_metrics_list(test_start_dates, res, para_info)
    sys_utils.print_metrics_list(best_start_dates, best_res, best_para_info)


main()

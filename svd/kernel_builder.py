
import svd_config
from gaussian_kernel import MiniGaussianKernel, GaussianKernel


def get_kernel(x, y, type, config={}):
    mini = False
    if max(len(x), len(y)) < svd_config.MINI_THRESHOLD:
        mini = True

    if type == 0:
        if mini:
            return MiniGaussianKernel(x, y, config)
        else:
            return GaussianKernel(x, y, config)

    raise TypeError("Unknow kernel type: " + str(type))


# def main():
    # from scipy import io as sio
    # mat = sio.loadmat(
    #     "/Users/cy/Downloads/tmp/stocks_2020/stock_core/tests/data/gaussian.mat")
    # X = mat['X']
    # Y = mat['Y']
    # K_gt = mat['K']
    # gamma = mat['gamma'][0][0]

    # kernel = get_kernel(X, Y, 0, {'gamma': gamma})
    # print(np.allclose(kernel.get_tile(0, 2, 4, 8),
    #                   K_gt[0:2, 4:8], equal_nan=True))
    # K = kernel.raw_data()
    # print(np.allclose(K, K_gt, equal_nan=True))

    # kernel[2:3]
    # print(np.allclose(kernel.get_col(2), K_gt[:, 2], equal_nan=True))
    # print(np.allclose(kernel.get_row(2), K_gt[2], equal_nan=True))

    # print(np.allclose(kernel.get_row(0), K_gt[0], equal_nan=True))
    # print(np.allclose(kernel.get_row(0, slice(2, 5)), K_gt[0, 2:5], equal_nan=True))
    # print(kernel.get_row(0, slice(2, 5)))
    # print(K_gt[0, 2:5])

    # print(kernel[:, 1].shape)
    # print(kernel[2:3, 3:4].shape)
    # K = kernel.raw_data()
#     print(np.allclose(K, K_gt, equal_nan=True))
    # kernel.save('/Users/cy/Downloads/tmp/stocks_2020/stock_core/tests/data/')


# main()

import os
import numpy as np
from scipy import stats
from stock_core.zoo import base
from stock_core.api import engine, date_config
from stock_core.zoo import regress_per_row
from stock_core import sys_utils
from stock_core.utils import general_evaluate
from kernel_builder import get_kernel

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def zscore_features(train_x, test_x):
    sigma = np.std(train_x, axis=1, ddof=1)
    train_x = stats.zscore(train_x, axis=1, ddof=1)
    np.nan_to_num(train_x, copy=False)
    test_x = test_x * (1 / sigma)[:, np.newaxis]
    test_x[sigma == 0] = 0

    sigma_cap = 3
    train_x[train_x > sigma_cap] = sigma_cap
    train_x[train_x < -sigma_cap] = -sigma_cap
    test_x[test_x > sigma_cap] = sigma_cap
    test_x[test_x < -sigma_cap] = -sigma_cap
    return train_x, test_x


class KernelRidgeLowRank(base.Model):
    def execute(train_y, train_x, test_x, model_config):
        tr_x_shape = train_x.shape
        tr_y_shape = train_y.shape
        te_x_shape = test_x.shape
        te_y_shape = (tr_y_shape[0], te_x_shape[1])

        train_x, test_x = zscore_features(train_x, test_x)

        # Currently, the reshape method is different
        # from Matlba version. But it only requires
        # to make sure that x matches y
        train_y = np.reshape(train_y.T, (tr_y_shape[0] * tr_y_shape[1], 1))
        train_x = np.reshape(
            train_x, (tr_y_shape[0] * tr_y_shape[1], tr_x_shape[0] // tr_y_shape[0]))
        test_x = np.reshape(
            test_x, (tr_y_shape[0] * te_x_shape[1], tr_x_shape[0] // tr_y_shape[0]))

        gamma = model_config['gamma']
        k_type = model_config['kernel_type']

        # Default evaluate function is stock_core.utils.general_evaluate
        # It returns a dict, which contains some default metrics
        # The pred_y and y requires to be reshaped before calling the function
        # Example:
        #
        # train_pred_y = some_fuc()
        # metrics = fn_evaluate(np.reshape(
        #     train_pred_y, tr_y_shape), np.reshape(train_y, tr_y_shape))
        # rho_u = metrics['rho_u']
        fn_evaluate = model_config['evaluate_function']

        k = get_kernel(train_x, train_x, k_type, {'gamma': gamma})
        k_test = get_kernel(test_x, train_x, k_type, {'gamma': gamma})

        # INPUT for Andreas's API:
        #   train_y, train_x, test_x, k, k_test, fn_evaluate
        #
        # Required OUTPUT:
        #   train_pred_y, test_pred_y
        #
        # The API also can return extra data such as
        # optimal lambda, optimal rank. These extra
        # data can return to user by saving to "user_data"

        train_pred_y = None
        test_pred_y = None

        optimal_lambda = None
        optimal_rank = None
        user_data = {'lambda': optimal_lambda,
                     'rank': optimal_rank
                     }

        train_pred_y = np.reshape(train_pred_y, tr_y_shape)
        test_pred_y = np.reshape(test_pred_y, te_y_shape)
        return train_pred_y, test_pred_y, user_data


def main():
    features = ['last_day_2pmto4pm_return',
                'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    train_response = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    test_response = train_response

    hyper_para_gamma = [0.1, 0.01, 0.001]

    # Setup engine
    e = engine.Engine()
    e.features(features)
    e.responses(train_response, test_response)
    e.universe('sadv_past_1y', 'max', 3000)
    e.models([KernelRidgeLowRank])
    # e.decorr_y('sadv_past63_day').postprocs('demean')

    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)

    tatol_res = []

    for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in d_f:
        # Setup date for a time range
        e.prepare_data(tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt)
        scan_res = {}
        # Scan gamma, use bayesopt later
        for gamma in hyper_para_gamma:
            model_output, metrics = e.run_models([{
                'gamma': gamma,
                'kernel_type': 0,
                'evaluate_function': general_evaluate
            }])
            scan_res[gamma] = (model_output, metrics)
            sys_utils.print_metrics(te_st_dt, metrics, {'gamma': gamma})
        tatol_res.append(scan_res)


main()

import os
from time import time
import numpy as np
from scipy import stats
from scipy.io import savemat
from stock_core import sys_utils, model_runner
from stock_core.api import engine, date_config
from kernel_builder import get_kernel
from low_rank import low_rank


CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def zscore_features(train_x, test_x):
    sigma = np.std(train_x, axis=1, ddof=1)
    train_x = stats.zscore(train_x, axis=1, ddof=1)
    np.nan_to_num(train_x, copy=False)
    test_x = test_x * (1 / sigma)[:, np.newaxis]
    test_x[sigma == 0] = 0

    sigma_cap = 3
    train_x[train_x > sigma_cap] = sigma_cap
    train_x[train_x < -sigma_cap] = -sigma_cap
    test_x[test_x > sigma_cap] = sigma_cap
    test_x[test_x < -sigma_cap] = -sigma_cap
    return train_x, test_x


def main():
    # features = ['last_day_2pmto4pm_return',
    #             'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    # train_response = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    features = ['last_day_2pmto4pm_return', 'sadv_past_63d']
    train_response = 'today_open_to_close_return - today_open_to_close_return[18596]'
    test_response = train_response
    universe = 50
    gammas = [10, 5, 1, 0.1, 0.01]

    # Setup engine
    e = engine.Engine()
    e.features(features)
    e.responses(train_response, test_response)
    e.universe('sadv_past_3y', 'max', universe)

    cache_folder = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), '3y_cache_' + str(universe))
    os.makedirs(cache_folder, exist_ok=True)

    data_folder = os.path.join(os.path.dirname(
        os.path.abspath(__file__)), 'data_' + str(universe))
    os.makedirs(data_folder, exist_ok=True)

    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)

    for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in d_f:
        e.prepare_data(tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt)
        p = model_runner.ModelRunner(e.train_resp_df,
                                     e.train_feat_dfs.values(),
                                     e.test_resp_df,
                                     e.test_feat_dfs.values(),
                                     [],
                                     [])
        stocks = np.asarray(e.sfw.get_stocks(), dtype=np.object)

        data = p.get_raw_inputs()
        train_y = data.train_y
        train_x = data.train_x
        test_y = data.test_y
        test_x = data.test_x

        _train_y = train_y
        _train_x = train_x
        _test_y = test_y
        _test_x = test_x

        tr_x_shape = train_x.shape
        tr_y_shape = train_y.shape
        te_x_shape = test_x.shape
        te_y_shape = (tr_y_shape[0], te_x_shape[1])

        train_x, test_x = zscore_features(train_x, test_x)

        _train_x_norm = train_x
        _test_x_norm = test_x

        train_y = np.reshape(train_y, (tr_y_shape[0] * tr_y_shape[1], 1))
        test_y = np.reshape(test_y, (tr_y_shape[0] * te_x_shape[1], 1))
        cancat_train_x = []
        cancat_test_x = []
        for i in range(0, tr_x_shape[0], tr_y_shape[0]):
            cancat_train_x.append(
                train_x[i:i + tr_y_shape[0]].reshape(train_y.shape))
            cancat_test_x.append(
                test_x[i:i + tr_y_shape[0]].reshape(tr_y_shape[0] * te_x_shape[1], 1))
        train_x = np.concatenate(cancat_train_x, axis=1)
        test_x = np.concatenate(cancat_test_x, axis=1)

        for gamma in gammas:
            cache_file = str(te_st_dt) + "_" + \
                str(gamma).replace('.', '_') + ".npz"
            cache_file = os.path.join(cache_folder, cache_file)
            ntrain = len(train_y)

            if os.path.exists(cache_file):
                print('load evecs from file:', cache_file, flush=True)
                data = np.load(cache_file)
                evecs = data['evecs']
                evals = data['evals']
                y1 = data['y1']
            else:
                print('start build kernel, problem size:', ntrain, flush=True)
                start_time = time()
                k = get_kernel(train_x, train_x, 0,
                               {'gamma': gamma}).raw_data()
                print('end build kernel, time:', '%.2f' %
                      (time() - start_time), flush=True)
                print('start solve svd, problem size:', ntrain, flush=True)
                start_time = time()
                evecs, evals = low_rank(k)
                y1 = evecs.T @ train_y
                print('end solve svd, time:', '%.2f' %
                      (time() - start_time), flush=True)
                np.savez(cache_file, evecs=evecs, evals=evals, y1=y1)

            mat = {'train_x': train_x, 'train_y': train_y, 'test_x': test_x, 'test_y': test_y, 'evecs': evecs, 'evals': evals, 'gamma': gamma,
                   'X': _train_x, 'y': _train_y, 'X_test': _test_x, 'y_test': _test_y, 'X_norm': _train_x_norm, 'X_test_norm': _test_x_norm, 'stocks': stocks}
            mat_f_name = os.path.join(data_folder, str(
                te_st_dt) + "_" + str(gamma).replace('.', '_') + ".mat")
            savemat(mat_f_name, mat)


main()

import os
import numpy as np
import scipy.io
from stock_core.api import engine, date_config
from stock_core.zoo import regress_per_row
from stock_core.zoo import base
from stock_core import sys_utils
from stock_core.utils import pack_yx, ModelInput
import mat73

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


class KernelRidgeLowRank(base.Model):
    def execute(train_y, train_x, test_x, model_config):
        mat = model_config['mat']
        new_idx = model_config['new_idx']
        old_idx = model_config['old_idx']

        f_train_y = train_y[new_idx]
        old_train_y = mat['ytrain']
        old_train_y = old_train_y.reshape(
            old_train_y.shape[0] // f_train_y.shape[1], f_train_y.shape[1])
        old_train_y = old_train_y[old_idx]
        assert(np.allclose(f_train_y, old_train_y))
        return train_y, np.random.rand(train_y.shape[0], test_x.shape[1]), []


def main():
    # features = ['last_day_2pmto4pm_return',
    #             'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    # train_response = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    features = ['last_day_2pmto4pm_return', 'sadv_past_63d']
    train_response = 'today_open_to_close_return - today_open_to_close_return[18596]'
    test_response = train_response
    universe = 1000
    sadv_y = 1

    # Setup engine
    e = engine.Engine()
    e.features(features)
    e.responses(train_response, test_response)
    e.universe('sadv_past_%dy' % sadv_y, 'max', universe)
    e.models([KernelRidgeLowRank])
    # e.decorr_y('sadv_past63_day').postprocs('demean')

    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)

    old_stocks_list = scipy.io.loadmat(
        '/scratch/ychen/svd/mat_files_for_matlab_algs/ohlcvt.mat', variable_names=['stocks_ohlcvt'])['stocks_ohlcvt']
    old_data_dir = "/scratch/ychen/svd/xy_reshape_nstock1000zs10"
    last_year = -1

    for tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt in d_f:
        # this_year = te_st_dt // 10000
        # if this_year == last_year:
        #     continue
        # last_year = this_year
        # Setup date for a time range
        e.prepare_data(tr_st_dt, tr_ed_dt, te_st_dt, te_ed_dt)
        mat = mat73.loadmat(os.path.join(
            old_data_dir, str(te_st_dt) + '.mat'))

        old_data = mat['keepIdIdx']
        old_stocks = old_stocks_list[old_data]
        new_stocks = np.asarray(e.sfw.get_stocks())
        old_stocks = np.char.strip(old_stocks).astype(new_stocks.dtype)
        same_stocks, old_idx, new_idx = np.intersect1d(
            old_stocks, new_stocks, return_indices=True)
        print("date:", te_st_dt, "old_stock_size:",
              old_stocks.shape[0], 'new_stocks_size:', new_stocks.shape[0], 'intersection_size:', same_stocks.shape)

        e.run_models([{'mat': mat, 'old_idx': old_idx, 'new_idx': new_idx}])

        ytest = e.test_resp_df.to_numpy()[new_idx]
        old_ytest = mat['ytest']
        old_ytest = old_ytest.reshape(
            old_ytest.shape[0] // ytest.shape[1], ytest.shape[1])[old_idx]
        assert(np.allclose(old_ytest, ytest))


main()

import os
import collections
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats.mstats import winsorize


def return_best_corr(path):
    files = os.listdir(path)
    res = {}
    for file in files:
        file_dir = os.path.join(path, file)
        metrics = np.load(file_dir)
        corr = metrics['rho_u'].item()
        gamma = metrics['gamma'].item()
        if gamma not in res or corr > res[gamma]:
            res[gamma] = corr
    return res


def return_data_frame(path):
    files = os.listdir(path)
    gammas = np.zeros(len(files))
    corrs = np.zeros(len(files))
    ranks = np.zeros(len(files))
    for idx, file in enumerate(files):
        file_dir = os.path.join(path, file)
        metrics = np.load(file_dir)
        corr = metrics['rho_u'].item()
        gamma = metrics['gamma'].item()
        rank = metrics['rank'].item()
        gammas[idx] = round(gamma, 4)
        corrs[idx] = corr
        ranks[idx] = rank
    df = pd.DataFrame()
    df['gamma'] = gammas
    df['rank'] = ranks
    df['corr'] = corrs
    df = df.pivot(index='gamma', columns='rank', values='corr')
    arr = winsorize(df.to_numpy(), limits=[0.05, 0.05])
    arr = np.asarray(arr)
    df = pd.DataFrame(data=arr, index=df.index, columns=df.columns)
    return df


def plot_gamma_corr(base_dir, title):
    dates = os.listdir(base_dir)
    dates.sort()

    fig_folder = os.path.join(base_dir, "gamma_fig")
    os.makedirs(fig_folder, exist_ok=True)

    for date in dates:
        if date.isnumeric() == False:
            continue
        dates_dir = os.path.join(base_dir, date)
        res = return_best_corr(dates_dir)
        res = collections.OrderedDict(sorted(res.items()))
        (x, y) = zip(*res.items())
        plt.plot(x, y, label=str(date))
        plt.plot(x, y, 'ro-', markersize=5)
        plt.title(title)
        plt.xlabel('gamma')
        plt.ylabel('corr')
        plt.legend()
        plt.savefig(os.path.join(fig_folder, str(date) + ".jpg"))
        plt.close()


def plot_heatmap(base_dir, title):
    dates = os.listdir(base_dir)
    dates.sort()

    fig_folder = os.path.join(base_dir, "heatmap_fig")
    os.makedirs(fig_folder, exist_ok=True)
    for date in dates:
        if date.isnumeric() == False:
            continue
        dates_dir = os.path.join(base_dir, date)
        df = return_data_frame(dates_dir)
        plt.pcolor(df)
        plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
        plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
        plt.colorbar()
        plt.locator_params(axis='x', nbins=10)
        plt.locator_params(axis='y', nbins=25)
        plt.xlabel('rank')
        plt.ylabel('gamma')
        plt.title(title)
        plt.savefig(os.path.join(fig_folder, str(date) + ".jpg"))
        plt.close()


def main():
    # plot_gamma_corr(
    #     '/scratch/ychen/svd_cache/3_scan_gamma_res_50_20210620220439')
    dirs = {'automl search gamma in [0.01, 100]': '/home/ychen/stock/stocks_2020/stock_core/svd/3_opt_res_50_20210613200949'}
    # dirs = {'gamma in (0, 50]': '/scratch/ychen/svd_cache/3_scan_gamma_res_50_20210621194001',
    #         'gamma in (0, 5]': '/scratch/ychen/svd_cache/3_scan_gamma_res_50_20210622211544',
    #         'gamma in (0, 1]': '/scratch/ychen/svd_cache/3_scan_gamma_res_50_20210623134629',}
            # 'automl search gamma in [0.01, 100]': '/home/ychen/stock/stocks_2020/stock_core/svd/3_opt_res_50_20210613200949'}
    for title, dir in dirs.items():
        plot_gamma_corr(dir, title)
        # plot_heatmap(dir, title)


main()

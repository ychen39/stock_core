import numpy as np
from numpy import linalg as LA
from scipy.linalg import eigh
import primme
import svd_config


def low_rank(k, max_rank=150):
    mini = False
    if max(k.shape[0], k.shape[1]) < svd_config.MINI_THRESHOLD:
        mini = True
    if mini:
        return low_rank_mini(k)
    else:
        return low_rank_primme(k)


def low_rank_mini(k, max_rank=150):
    max_rank = min(len(k), max_rank)
    evals, evecs = LA.eig(k)

    evals = evals.view(np.float64)[::2]
    evecs = evecs.view(np.float64)[:, ::2]

    # num = np.where(evals < evals[0] * magnitude_drop)[0][0] + 1
    # num = min(num, max_rank)
    num = max_rank

    evecs = evecs[:, 0:num]
    evals = evals[0:num]
    return evecs, evals


def low_rank_primme(k, max_rank=150):
    max_rank = min(k.shape[0], max_rank)
    evals, evecs = primme.eigsh(
        k, max_rank, tol=1e-8, which='LA', method='PRIMME_DEFAULT_MIN_MATVECS', maxBlockSize=64)

    # num = np.where(evals < evals[0] * magnitude_drop)[0][0] + 1
    # num = min(num, max_rank)
    num = max_rank

    evecs = evecs[:, 0:num]
    evals = evals[0:num]
    return evecs, evals

# def main():
#     from scipy import io as sio
#     mat = sio.loadmat(
#         "/stocks_2020/stock_core/tests/data/svd.mat")
#     k = mat['K']
#     # evecs_gt = mat['evecs']
#     # evals_gt = mat['evals']

#     evecs, evals = low_rank(k, 50)
#     evecs_gt, evals_gt = low_rank_primme(k, 50)
#     print(evals - evals_gt)
#     print(np.allclose(evals, evals_gt))
#     # print(np.allclose(np.abs(evecs_gt.reshape(evecs.shape)), np.abs(evecs), equal_nan=True))
#     # print(np.allclose(evals, evals_gt.reshape(evals.shape), equal_nan=True))

# main()

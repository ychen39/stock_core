import os
from math import sqrt
import numpy as np
from stock_core import utils
from stock_core.sys_utils import print_metrics_list


# f_name = "/home/ychen/stock/stocks_2020/stock_core/svd/res_50/20110101/2d8b7861-3337-48df-8d3e-49618eeb0f31.npz"

def return_best_metrics(base_dir, best_col):
    dates = os.listdir(base_dir)
    dates.sort()
    best_metrics_list = []
    paras_list = []
    for date in dates:
        dates_dir = os.path.join(base_dir, date)
        res_list = os.listdir(dates_dir)
        best = -9999
        best_metrics = None
        for res_file in res_list:
            f_name = os.path.join(dates_dir, res_file)
            metrics = np.load(f_name)
            if best_metrics is None or metrics[best_col] > best:
                best_metrics = metrics
                best = metrics[best_col]
        best_metrics = dict(best_metrics)
        for k, v in best_metrics.items():
            if v.shape == ():
                best_metrics[k] = v.item()
        paras = {'gamma': best_metrics['gamma'],
                 'lamda': best_metrics['lamda'],
                 'rank': best_metrics['rank'],}
                #  'ntrain': best_metrics['ntrain'],
                #  'min_eigv': best_metrics['min_eigv']}
        best_metrics_list.append(best_metrics)
        paras_list.append(paras)
    return dates, best_metrics_list, paras_list


def combine_res(raw_metrics):
    combined_metrics = {}

    for i in range(0, len(raw_metrics)):
        for k, v in raw_metrics[i].items():
            if k not in combined_metrics:
                combined_metrics[k] = []
            combined_metrics[k].append(v)

    rg_rho_u = np.asarray(combined_metrics['rho_u'])
    rg_rho_iu = np.asarray(combined_metrics['rho_iu'])
    r_u_glued = np.concatenate(combined_metrics['u_glued'])
    nw_rgbeta_u_g = np.concatenate(combined_metrics['nw_rgbeta_u_g'])

    rho_u_glued = np.mean(rg_rho_u)
    rho_iu_glued = np.mean(rg_rho_iu)
    sharpe_u_glued = np.mean(r_u_glued) / np.std(r_u_glued) * sqrt(252)
    nwbeta_u_glued, nwse_u_glued, nwts_u_glued = utils.neweywest(
        nw_rgbeta_u_g)

    metrics_glude = {}
    metrics_glude['rho_u'] = rho_u_glued
    metrics_glude['rho_iu'] = rho_iu_glued
    metrics_glude['sharpe_u'] = sharpe_u_glued
    metrics_glude['nwbeta_u'] = nwbeta_u_glued
    metrics_glude['nwse_u'] = nwse_u_glued
    metrics_glude['nwts_u'] = nwts_u_glued

    avg_nnz_glude = np.asarray(combined_metrics['avg_nnz'])
    metrics_glude['avg_nnz'] = np.mean(avg_nnz_glude)
    stock_mask = np.zeros_like(combined_metrics['stock_mask'][0])
    for mask in combined_metrics['stock_mask']:
        stock_mask = stock_mask | mask
    metrics_glude['nstocks'] = np.sum(stock_mask[stock_mask > 0])
    return metrics_glude


def main():
    # res = np.load(f_name)
    # print(list(res.keys()))
    base_dir = "stock/stocks_2020/stock_core/svd/1_opt_res_50_20210614141101/"
    dates, metrics_list, paras_list = return_best_metrics(base_dir, 'rho_u')
    metrics_list.append(combine_res(metrics_list))
    dates.append('all')
    paras_list.append({'gamma': 'None',
                       'lamda': 'None',
                       'rank': 'None',})
                    #    'ntrain': 'None',
                    #    'min_eigv': 'None'})
    print_metrics_list(dates, metrics_list, paras_list)


main()

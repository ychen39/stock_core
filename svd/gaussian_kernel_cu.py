import math
from scipy import io as sio
import skcuda.linalg as linalg
import numpy as np
from pycuda import compiler, gpuarray
import pycuda.autoinit

linalg.init()

kernel_code = """
#include <math.h>

__global__ void gaussian_kernel(double *x, double *y, double *res, double gamma, int n, int m, int d)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;
    int idx = i * m + j;
    if (i >= n || j >= m) return;
    double tmp_sum = 0.0;
    for (int k = 0; k < d; k++)
    {
        tmp_sum += powf(x[i * d + k] - y[j * d + k], 2);
    }
    res[idx] = expf(-tmp_sum * gamma);
}
"""

core_fuc = compiler.SourceModule(kernel_code).get_function('gaussian_kernel')


def k_gpu(x_d, y_d, v_d, res_d, gamma, n, m, d):
    block_sz_x = 32
    block_sz_y = 32
    grid_sz_x = math.ceil(n / block_sz_x)
    grid_sz_y = math.ceil(m / block_sz_y)

    core_fuc(x_d, y_d, res_d, np.float64(gamma), np.int32(n), np.int32(
        m), np.int32(d), block=(block_sz_x, block_sz_x, 1), grid=(grid_sz_x, grid_sz_y, 1))
    return linalg.dot(res_d, v_d)


def k_gpu_standalone(x, y, v, gamma):
    n = len(x)
    m = len(y)
    l = n * m
    assert(x.shape[1] == y.shape[1])
    d = x.shape[1]

    block_sz_x = 32
    block_sz_y = 32
    grid_sz_x = math.ceil(n / block_sz_x)
    grid_sz_y = math.ceil(m / block_sz_y)

    # res = np.zeros((n, m))

    if v.strides != x.strides:
        tmp = np.zeros(v.shape)
        tmp[:] = v[:]
        v = tmp

    x_d = gpuarray.to_gpu(x.flatten())
    y_d = gpuarray.to_gpu(y.flatten())
    v_d = gpuarray.to_gpu(v)
    # res_d = gpuarray.to_gpu(res)
    res_d = gpuarray.empty((n, m), np.float64)

    core_fuc(x_d, y_d, res_d, np.float64(gamma), np.int32(n), np.int32(
        m), np.int32(d), block=(block_sz_x, block_sz_x, 1), grid=(grid_sz_x, grid_sz_y, 1))
    return linalg.dot(res_d, v_d).get()

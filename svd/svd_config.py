MINI_THRESHOLD = 20000  # 1524MB
# MINI_THRESHOLD = -1

KB = 1024
MB = KB * 1024
GB = MB * 1024

MAX_CACHE_SIZE = 14 * GB


def calculate_block_size(m, dtype_sz):
    row_sz = m * dtype_sz
    n = MAX_CACHE_SIZE // row_sz - 1
    n |= n >> 1
    n |= n >> 2
    n |= n >> 4
    n |= n >> 8
    n |= n >> 16
    return (n + 1) >> 1

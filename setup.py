import setuptools

setuptools.setup(
    name='stock_core',
    version='0.0.1.210314_alpha',
    packages=setuptools.find_packages(exclude=["tests", "example", "svd"]),
    python_requires='>= 3.4',
    install_requires=['numpy', 'pandas', 'scipy',
                      'prettytable', 'python-dateutil', 'pyarrow'],
)

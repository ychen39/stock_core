import os
from stock_core.api import data_manager
from stock_core import sys_utils

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')
os.environ[sys_utils.DEFAULT_DATA_MANAGER_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'data_manager')


def main():
    start_date = 20071201
    end_date = 20101201
    df = data_manager.get_data(start_date, end_date, query_key="3874300295",
                               sadv_date=end_date, universe_size=3000, cache=True)
    print(df)


main()

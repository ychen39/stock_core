import os
import numpy as np
from stock_core import features, evaluator, sys_utils, utils
from stock_core.api import data_manager

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')
os.environ[sys_utils.DEFAULT_DATA_MANAGER_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'data_manager')


def regress_per_row(train_y, train_x, test_x, model_config):
    iter_num = len(train_y)
    x_row_per_iter = int(len(train_x) / iter_num)
    x_indexes = np.zeros((x_row_per_iter,), dtype=int)
    for i in range(1, x_row_per_iter):
        x_indexes[i] = x_indexes[i-1] + iter_num

    train_pred_y = np.zeros_like(train_y)
    test_pred_y = np.zeros((iter_num, test_x.shape[1]))

    for i in range(0, iter_num):
        t_x = train_x[x_indexes]
        tb = np.linalg.lstsq(t_x.T, train_y[i].T, rcond=None)[0]
        train_pred_y[i] = (t_x.T @ tb).T
        test_pred_y[i] = (test_x[x_indexes].T @ tb).T
        x_indexes += 1

    np.nan_to_num(test_pred_y, nan=0, copy=False)
    test_pred_y[np.isinf(test_pred_y)] = 0
    return train_pred_y, test_pred_y


def main():

    data_names = ['last_day_2pmto4pm_return', 'last_day_2pmto4pm_return*sadv_past_63d',
                  'sadv_past_63d', 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930']

    train_start_date = 20071201
    train_end_date = 20101201
    test_start_date = 20110101
    test_end_date = 20110401

    query_key = data_manager.build_query_data(data_names)

    # df = data_manager.get_data(start_date, end_date, 'top3000_everyyear')
    # df = data_manager.get_data(start_date, end_date, symbols)

    train_df = data_manager.get_data(train_start_date, train_end_date, query_key=query_key,
                                     sadv_date=train_end_date, universe_size=3000, cache=True)

    test_df = data_manager.get_data(test_start_date, test_end_date, query_key=query_key,
                                    sadv_date=train_end_date, universe_size=3000, cache=True)

    train_df, test_df = data_manager.remove_junky_stocks(
        train_df, test_df, data_names)
    # print('Current stocks number:', train_df['stock'].nunique())

    train_y, train_x = data_manager.pack_xy(
        train_df, data_names[-1], data_names[:-1])
    test_y, test_x = data_manager.pack_xy(
        test_df, data_names[-1], data_names[:-1])

    # print(train_y.shape, train_x.shape)
    # print(test_y.shape, test_x.shape)

    train_pred_y, test_pred_y = regress_per_row(train_y, train_x, test_x, {})
    # Or use the model in zoo
    # from stock_core.zoo import regress_per_row
    # train_pred_y, test_pred_y = regress_per_row.execute(train_y, train_x, test_x, {})

    # Use evaluator to do some basic evaluation
    model_output = utils.ModelOutput(
        train_y, train_pred_y, test_y, test_pred_y)
    e = evaluator.Evaluator(model_output)
    metrics = e.evaluate().get_metrics()  # Metrics is a dict

    sys_utils.print_metrics(test_start_date, metrics)


main()

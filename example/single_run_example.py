import os
from stock_core.api import engine
from stock_core.zoo import regress_per_row
from stock_core import sys_utils

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def main():
    e = engine.Engine()
    e.features(['last_day_2pmto4pm_return',
                'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d'])
    e.responses('raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930',
                'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930')
    e.universe('sadv_past_1y', 'max', 3000)
    e.models([regress_per_row.RegressPerRow])
    # These two are optional
    # e.decorr_y('sadv_past63_day').postprocs('demean')
    model_output, metrics = e.single_run(
        20071201, 20101201, 20110101, 20110401)
    # print(model_output.train_y, model_output.train_pred_y,
    #   model_output.test_y, model_output.test_pred_y)

    sys_utils.print_metrics(20110101, metrics)


main()

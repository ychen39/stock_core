import os
from stock_core.api import engine, date_config
from stock_core.zoo import regress_per_row
from stock_core import sys_utils

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')

'''
User define model:
from stock_core.zoo import base
class MyModel():
    def execute(train_y, train_x, test_x):
        do something
        return train_pred_y, test_pred_y

e.models([regress_per_row.RegressPerRow, MyModel])
'''


def main():
    e = engine.Engine()
    e.features(['last_day_2pmto4pm_return',
                'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d'])
    e.responses('raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930',
                'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930')
    e.universe('sadv_past_1y', 'max', 3000)
    e.models([regress_per_row.RegressPerRow])
    # These two are optional
    # e.decorr_y('sadv_past63_day').postprocs('demean')
    d_f = date_config.DateConfig(
        20071201, 20190101, test_months=3, gray_months=1, train_years=3)
    _, metrics_list = e.run(d_f)
    test_date_list = d_f.get_test_start_dates() + ['all']
    sys_utils.print_metrics_list(test_date_list, metrics_list)


main()

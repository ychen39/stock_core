import os
from stock_core.api import data_manager
from stock_core import sys_utils

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')
os.environ[sys_utils.DEFAULT_DATA_MANAGER_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'data_manager')


def main():
    print("Available data: ")
    print(data_manager.get_data_list())
    data_names = ['last_day_2pmto4pm_return', 'last_day_2pmto4pm_return',
                  'sadv_past_63d', 'raw_lead_next_1d_at_930']
    query_key = data_manager.build_query_data(data_names)
    print("query key:", query_key)


main()

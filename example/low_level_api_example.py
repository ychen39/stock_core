import os
from stock_core.zoo import regress_per_row
from stock_core import features, filters, model_runner, evaluator, sys_utils
import prettytable as pt

CURRENT_PATH = os.path.abspath(__file__)
STOCK_LIB_PATH = os.path.abspath(
    os.path.dirname(CURRENT_PATH) + os.path.sep + "..")
os.environ[sys_utils.DEFAULT_FEATURE_MAT_DIR_STR] = os.path.join(
    STOCK_LIB_PATH, 'data', 'mat')


def main():
    features_names = ['last_day_2pmto4pm_return',
                      'last_day_2pmto4pm_return*sadv_past_63d', 'sadv_past_63d']
    train_response_names = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    test_response_names = 'raw_lead_next_1d_at_930 - raw_sp500_lead_next_1d_at_930'
    # Get feat objects
    f = features.FeatureCenter()
    feat_objs = {feature_name: f.get_expr_feature(
        feature_name) for feature_name in features_names}
    train_response = f.get_expr_feature(train_response_names)
    test_response = f.get_expr_feature(test_response_names)

    train_start_date = 20071201
    train_end_date = 20101201
    test_start_date = 20110101
    test_end_date = 20110401

    # Query data by dates
    train_feat_dfs = {k: v.get_feature_data(
        train_start_date, train_end_date) for k, v in feat_objs.items()}
    test_feat_dfs = {k: v.get_feature_data(
        test_start_date, test_end_date) for k, v in feat_objs.items()}

    train_resp_df = train_response.get_feature_data(
        train_start_date, train_end_date)
    test_resp_df = test_response.get_feature_data(
        test_start_date, test_end_date)

    # All data should have same stock lists
    # Here we use response's stocks
    stocks = train_response.get_stocks()

    sfw = filters.StockFilterWrapper(stocks)

    # Mark stocks with all NaN values in either one data
    # Filter name is 'dropnan'
    sfw.update_mask('dropnan', list(train_feat_dfs.values()) +
                    list(test_feat_dfs.values()) + [train_resp_df, test_resp_df])

    # Mark stocks in blacklist
    # Blacklist is defined in sys_utils.BLACK_STOCKS_HEAD
    sfw.update_mask('BL', (stocks, ))

    # Mark stocks based on Universe
    op = 'max'  # or 'min'
    threadshold = 3000
    sadv = f.get_expr_feature(
        'sadv_past_1y').get_feature_data_lastday(train_end_date)
    sfw.update_mask('rank', (sadv, op, threadshold))

    # Another way:
    # Mutiple filters can be applied at some time
    # by using update_mask calling chain
    #
    # sfw.update_mask('dropnan',
    #     list(train_feat_dfs.values())
    #     ).update_mask('dropnan',
    #     list(test_feat_dfs.values())
    #     ).update_mask('dropnan',
    #     [train_resp_df, test_resp_df]
    #     ).update_mask('BL',
    #     (stocks, )
    #     ).update_mask('rank',
    #     (sadv, op, threadshold))

    # Use mask to filter data
    train_feat_dfs = {k: sfw.filter(v) for k, v in train_feat_dfs.items()}
    test_feat_dfs = {k: sfw.filter(v) for k, v in test_feat_dfs.items()}
    train_resp_df = sfw.filter(train_resp_df)
    test_resp_df = sfw.filter(test_resp_df)

    # Check current stocks
    # print(len(sfw.get_stocks()))

    # Init runner
    p = model_runner.ModelRunner(train_resp_df,
                                 train_feat_dfs.values(),
                                 test_resp_df,
                                 test_feat_dfs.values(),
                                 [regress_per_row.RegressPerRow],
                                 [None])

    # Preprocess before running
    # Get decorr field
    # M_sadv63d = f.get_expr_feature('sadv_past63_day')
    # train_field = M_sadv63d.get_feature_data(train_start_date, train_end_date)
    # test_field = M_sadv63d.get_feature_data(test_start_date, test_end_date)
    # Use same filter to make shape consistant
    # train_field, test_field = sfw.filter(train_field, test_field)
    # do decorr
    # p.execute_preprocs('decorr', (train_field, test_field, 63))

    # Run models
    p.run()

    # Init evaluator
    t = evaluator.Evaluator(p.get_combined_result())

    # Do post process
    # force long short on equal num_stocks to improve sharpe and returns
    # t.execute_postprocs('demean')

    # Other post process plugin
    # t.execute_postprocs('long_only_plugin')

    t.evaluate()
    metrics = t.get_metrics()  # metrics is a dict
    sys_utils.print_metrics(test_start_date, metrics)

    # tb = pt.PrettyTable()
    # tb.field_names = ["test_start_day",
    #                   "N",
    #                   "avg_nnz",
    #                   "rho_u",
    #                   "sharpe_u",
    #                   "rho_in_u",
    #                   "nw_beta_u",
    #                   "nw_tstat_u"]

    # tb.add_row([test_start_date,
    #             metrics['nstocks'],
    #             metrics['avg_nnz'],
    #             round(metrics['rho_u'], 4),
    #             round(metrics['sharpe_u'], 4),
    #             round(metrics['rho_iu'], 4),
    #             round(metrics['nwbeta_u'], 4),
    #             round(metrics['nwts_u'], 4)
    #             ])

    # print(tb)


main()

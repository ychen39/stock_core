# README #


### How do I get set up? ###

```
# python3 setup.py install
# cd data;sh download_data.sh;
```

### Run examples one by one ###

##### Example of data loading with data manager:

```
# cd example
# python3 build_query_data.py
# python3 data_load.py
```
##### Example of running a model with data manager:

```
# cd example
# python3 simple_example.py
```

##### Example of high-level APIs (single data range):
```
# cd example
# python3 single_run_example.py
```

##### Example of high-level APIs (multiple data ranges):
```
# cd example
# python3 date_run_example.py
```

##### Example of using low-level APIs:
```
# cd example
# python3 low_level_api_example.py
```

NOTICE: First call to functions in data manager can be slow but results will be cached.